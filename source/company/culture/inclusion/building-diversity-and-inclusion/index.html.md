---
layout: markdown_page
title: "Building an Inclusive Remote Culture"
---

## On this page
{:.no_toc}

- TOC
{:toc}

#### Introduction

We are passionate about [all remote working](/company/culture/all-remote/vision/) and enabling an [inclusive work](/company/culture/inclusion/#fully-distributed-and-completely-connected) environment. There isn't one big activity we can take to accomplish this. Instead, it is a mix of numerous [activities and behaviors](/blog/2019/12/06/how-all-remote-supports-inclusion-and-bolsters-communities/) combined to enable our team members feel they belong in GitLab.

Those activities and behaviors include:

* [D&I Events](/company/culture/inclusion/diversity-and-inclusion-events/)
* [D&I Advisory Group](/company/culture/inclusion/#understanding-of-the-purpose-of-the-global-diversity--inclusion-advisory-group)
* [Diversity & Inclusion ERGs - Employee Resource Groups](/company/culture/inclusion/#ergs---employee-resource-groups)
* [Our Values](/handbook/values/)
* [Family and Friends first, Work second](/handbook/values/#family-and-friends-first-work-second)
* [Inclusive language and pronouns](/handbook/values/#inclusive-language--pronouns)
* [Parental leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave) 
* [Asynchronous communication and workflows](/company/culture/all-remote/asynchronous/)

## Tips for Companies  

### Defining Diversity & Inclusion

A great place to begin is to set a foundation of the basic understanding of how your company defines these terms. An example is GitLab's [Diversity & Inclusion value](/handbook/values/#diversity-inclusion), supported by an evolving list of sub-values.

Most places you will hear them used interchangeably. Understanding they are different is essential to driving the initiatives. As an example, you can hire as many diverse candidates but if you don't create an inclusive environment the work can be in vain.  

### Evaluating the companies current D&I landscape

Consider what you are already doing in this space.

* What is the current feedback?
* What are team members saying with company engagement surveys?
* What are the goals you are wanting to achieve?
* What are the metrics saying?

### Naming this body of work

Although Diversity & Inclusion are often understood globally there are other terms that can be leveraged to name your efforts. The naming should be unique to your company. Examples could include Belonging, Inclusion & Collaborations, etc.

### Developing a mission statement

When creating a diverse and inclusive culture, most companies will develop a mission statement to support their vision. Your mission statement should articulate the purpose of your strategy. In a few sentences you should be able to succinctly provide the why and the how. Be sure to take into account your company’s current overall mission and vision. It is best to align your D&I mission and vision with your organization’s overarching mission and vision. To do this, you may consider how your D&I Strategy can build on, scale, and or enhance the organization’s mission and vision.

### Creating ERGs

In general, ERGs are an excellent support system and key to providing awareness, respect, and building diversity and inclusion within the workplace. These groups are a proven way to increase cultural competency, retention of team members, provide marketplace insights to the business, attract diverse talent, and more. The endorsement of ERGs gives team members the opportunity to identify common interests, and decide how they can be shared with others.  When creating ERGs there are few initial steps to consider:
* [Creating guidelines to help support ERGs being stood up within your company](/company/culture/inclusion/erg-guide/)
    * Naming of each ERG
    * Roles within each ERG
    * Aligning to company strategy
* Creating forms, google groups ways of tracking attendance for ERG events and membership metrics

### Creating a Diversity & Inclusion Advisory Group

Consider creating this group as your highest level of global voices. A team of company influencers who can be instrumental in driving D&I efforts from a global perspective. How do you do this? GitLab conducted a global "All Call" for those who would be interested in joining and advised to provide the "why" D&I is important to them along with others questions such as division, location, etc. so that when we were reviewing we were able to have the best possible outcome of representation across the globe. Additional support in sustaining the group would be:
* [D&I Advisory group guidelines](https://about.gitlab.com/company/culture/inclusion/advisory-group-guide/)
* Appointing an Executive sponsor from the company
* Designating leads of the group
* Deciding on when it is time to enact and or rotate the opportunity for new advisory group members

### More to come

* D&I Initiatives
* D&I Awards for Recognition
* D&I Surveys
* D&I Framework Inclusive Benefits

## Tips for Managers

### Set aside time to show up for planned D&I events

You might be surprised by how much seeing your face in these events validates to others that this is a worthwile use of time that is valued by the company. Seeing you in attendance also opens to the door to future conversations that might begin "I saw you in XYZ meeting, what did you think of such-and-such topic?"

### Incorporate D&I into your team meetings

Being able to connect to your team members is key in understanding who they are and what they bring to the team. This can be done in several ways but a great initial step is to start with open discussions. You could start by opening your next team meeting to chat about what they feel inclusion looks like, what is important to them as a team to feel included, etc. This could then move into monthly or quarterly team D&I icebreakers, trainings etc. The idea is to make sure D&I is not touched on once and never mentioned again but more of an understood aspect of your team environment. You can do this by setting and communicating D&I goals for your team, and sharing how we will measure success (an idea could be sharing D&I survey data with team).

### Encourage Diversity of Thought and Speaking Up

Don’t assume you know more than others, give people a chance to add their perspective or expertise. Teach people how to disagree, and set the expectation that it is okay to disagree, and encourage people to do it day to day. Celebrate diversity of thought, and thank people for making contributions, particularly if they disagree with you. It takes guts to disagree publicly with a leader.

### Connect with your team members

To help team members to feel comfortable being themselves, leadership should consider authentic ways to connect to their team members. At it's highest level everyone wants to feel visbile and included in order to perform their best work.  Being visible usually includes being seen for accomplishments, acknowledgement of contributions, what is the same and what differs from team member to team member.  Leaders and all team members should show an interest in and respect for differences and contributions.

Considerations could be that of language/terminology such as “spouses” or “partners” instead of making assumptions about team members sexual orientation.  Being considerate of dietary restrictions when choosing food options for Contribute or other local gatherings.  Acknowleding birthdays, recalling personal things that might have been mentioned in past calls such as moving to a new home, an ill family member or team member themselve and following up to inquire in an authentic way.

For more information see our [Inclusive Language](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)

### Ask employees what pronouns they use

Pronouns are a large piece of a person's identity and are often used to communicate a person’s gender, which is why it is so important to get it right. Asking for a person's pronouns and using those pronouns consistently shows that you respect their identity, but it also helps to create a more welcoming, safe and supportive environment where people can feel comfortable to be themselves. This is a change that goes a long way to foster inclusion.

### Be mindful of the times of your meetings

It's important to consider global times and alternating meetings to accomodate regions. Be mindful of families, commitments, observances, holidays, and meeting times which may be out of a team members working hours. Every meeting time won't be perfect for everyone (which is why all meetings [have an agenda](/company/culture/all-remote/meetings/#have-an-agenda)), but making a conscious effort to alternate times is to ensure the same people aren't being excluded. For more, view our [Inclusive Meetings](/handbook/values/#inclusive-meetings) sub-value.

### Be a Role Model

As a manager, you are in a unique position to model good D&I practices for the rest of your team. **Be authentic, own up to mistakes and commit to do better next time. Don’t dwell on it.** Set a good example by admitting that you make mistakes, invite poeple to help you and each other by speaking up in accordance with our feedback guidelines. By holding each other accountable, we get better as a team.  

## Tips for Team Members

### Schedule a Coffee Chat with a Team Member

Understanding GitLab is fully remote there is an opportunity to get to know team members beyond your geographical location as well as outside of your division.  This will provide you an opportunity to learn more about other cultures, work divisions, cultivate better global communication and overall continuing to work towards building an inclusive environment.  Coffee chats are suggested during onboarding but don't need to stop there.  It is recommended to continue this action whenever you would like to.  Please take a look at our [GitLab team page](https://about.gitlab.com/company/team/) and feel free to select someone for a coffee chat!

### Hold Each Other Accountable

Hold each other accountable and speak up when someone uses non-inclusive language. We should also celebrate when we are inclusive, and strive to support each other every day.

### Tip to Uncover Unconscious Bias

When you are dealing with a specific situation, you can mentally flip the situation around and see how that feels. For example, flip it from a woman to a man, and if it feels off then you might have a bias. By putting ourselves in someone else's shoes, we can better understasnd their point of view and be more supportive.

### More to come

* Consider joining an ERG (Employee Resource Group)
* Support others as an Ally
* Review the Diversity & Inclusion page
