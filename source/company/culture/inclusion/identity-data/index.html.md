---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2020-04-30

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 131       | 10.40%          |
| Based in EMEA                               | 332       | 26.35%          |
| Based in LATAM                              | 18        | 1.43%           |
| Based in NORAM                              | 779       | 61.83%          |
| **Total Team Members**                      | **1,260** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 887       | 70.40%          |
| Women                                       | 372       | 29.52%          |
| Other Gender Identities                     | 1         | 0.08%           |
| **Total Team Members**                      | **1,260** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 62        | 73.81%          |
| Women in Leadership                         | 22        | 26.19%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **84**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 431       | 81.78%          |
| Women in Development                        | 95        | 18.03%          |
| Other Gender Identities                     | 1         | 0.19%           |
| **Total Team Members**                      | **527**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.27%           |
| Asian                                       | 48        | 6.57%           |
| Black or African American                   | 21        | 2.87%           |
| Hispanic or Latino                          | 40        | 5.47%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 26        | 3.56%           |
| White                                       | 439       | 60.05%          |
| Unreported                                  | 155       | 21.20%          |
| **Total Team Members**                      | **731**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 19        | 9.13%           |
| Black or African American                   | 3         | 1.44%           |
| Hispanic or Latino                          | 9         | 4.33%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.37%           |
| White                                       | 133       | 63.94%          |
| Unreported                                  | 37        | 17.79%          |
| **Total Team Members**                      | **208**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 13.64%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 3.03%           |
| White                                       | 40        | 60.61%          |
| Unreported                                  | 15        | 22.73%          |
| **Total Team Members**                      | **66**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.16%           |
| Asian                                       | 113       | 8.97%           |
| Black or African American                   | 30        | 2.38%           |
| Hispanic or Latino                          | 64        | 5.08%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 36        | 2.86%           |
| White                                       | 702       | 55.71%          |
| Unreported                                  | 313       | 24.84%          |
| **Total Team Members**                      | **1,260** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 54        | 10.25%          |
| Black or African American                   | 7         | 1.33%           |
| Hispanic or Latino                          | 26        | 4.93%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 14        | 2.66%           |
| White                                       | 297       | 56.36%          |
| Unreported                                  | 129       | 24.48%          |
| **Total Team Members**                      | **527**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 11.90%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.19%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 2.38%           |
| White                                       | 49        | 58.33%          |
| Unreported                                  | 22        | 26.19%          |
| **Total Team Members**                      | **84**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 16        | 1.27%           |
| 25-29                                       | 229       | 18.17%          |
| 30-34                                       | 347       | 27.54%          |
| 35-39                                       | 274       | 21.75%          |
| 40-49                                       | 273       | 21.67%          |
| 50-59                                       | 107       | 8.49%           |
| 60+                                         | 13        | 1.03%           |
| Unreported                                  | 1         | 0.08%           |
| **Total Team Members**                      | **1,260** | **100%**        |


Source: GitLab's HRIS, BambooHR
