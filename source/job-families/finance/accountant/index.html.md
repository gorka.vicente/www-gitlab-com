---
layout: job_family_page
title: "Accounting"
---

## Accountant

### Job Grade

The Accountant is a [grade 6](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Manage full-cycle accounts payable process including vendor and invoice management, approval and weekly disbursement activities.
- Manage expense reports, credit card account reconciliations and payable side bank reconciliation.
- Compile and prepare entries to general ledger accounts, providing support that is organized, accurate, and descriptive.
- Prepare balance sheet reconciliations on a monthly basis for assigned accounts, identifying and resolving any differences within the close period. Reconciliations must be organized, accurate, compete. Full understanding of account in accordance with GAAP is necessary.
- Fulfill all duties, including journal entries, reconciliations, and other assigned tasks, in a timely manner to comply with the close calendar, - - checklists, and other due dates as assigned.
- Implementing and maintaining documented system of accounting policies and procedures
- Assist with investor and bank reporting.
- Perform ad hoc analysis and process improvement project

### Requirements

- Must have experience with Netsuite
- Proficient with Excel and Google Sheets
- International experience is a plus
- Self-starter with the ability to work remotely and independently and interact with various teams when needed.
- You share our [values](/handbook/values), and work in accordance with those values.
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
- Ability to use GitLab

## Senior Accountant

### Job Grade

The Senior Accountant is a [grade 7](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Support the external audit processes, working directly with the external auditors and internal team members from Technical Accounting and Accounting Operations.
* Collaborate with local service providers for each operating entity to ensure compliance with statutory and tax reporting.
* Support the efficient, accurate, and timely production of consolidated financial statements.
* Perform tasks as needed to support the possibility of a public filing, which would include Form S-1 and the related subsequent SEC reporting requirements including Forms 10-K, 10-Q, 8-K and Proxy filings.
* Ensure compliance with Sarbanes-Oxley Section 404 key controls in the financial areas of responsibility, as applicable.
* Assist with monitoring the need for business process improvements and assist with the design processes, procedures, and reporting enhancements to improve financial and operational processes.
* Support the design and implementation of new policies and procedures related to audit requirements and business activities.
* Assist with preparation of reports and presentations for Board of Directors and Audit Committee meetings.
* Respond to inquiries from the CFO, Controller, and company wide managers regarding financial results, special reporting requests and the like.
* Work with and support the accounting team in day-to-day activities, special projects, and workflow process improvements.
* Support overall department goals and objectives.
* This position reports to the Accounting and External Reporting Manager.

### Requirements

* 3-5 years experience in public accounting, preferably with a large firm, and/or in financial reporting at a US public company
* Experience with SEC companies required.  Public accounting experience must include working on SEC clients.
* Software company experience preferred
* IPO experience preferred
* Some tax experience preferred but not required
* Strong working knowledge of GAAP principles
* Experience with Netsuite or other big ERP system preferred but not required
* Proficient with Microsoft Office suite and/or Google Docs and Sheets
* Experience working with international subsidiaries, including statutory reporting requirements, strongly preferred
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
- You share our [values](/handbook/values), and work in accordance with those values.
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).

## Manager, Accounting

### Job Grade

The Manager, Accounting is a [grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Assist accounting staff on daily tasks, as needed.
* Provide training to new and existing staff, as needed.
* Ensure an accurate and timely month end close by providing leadership and support to the accounting close process. This role includes serving as a hands-on team member responsible for performing and reviewing transactions which includes but not limited to equity, intercompany and allocations.
* Assist with daily banking requirements.  Monitor and forecast cash position
* Assist in implementation of new procedures to enhance the workflow of the department.
* Enforce proper accounting policies and principles.
* Responds to inquiries from the CFO, Controller, and company wide managers regarding financial results, special reporting requests and the like.
* Assist in preparing for annual external audits.  Coordinate with other auditors as needed.
* Support overall department goals and objectives

### Requirements

* Proven work experience as an Accounting Manager or similar leadership role.
* Public company accounting experience is required
* Ability to contribute to the career development of staff and a culture of teamwork
* Strong working knowledge of GAAP principles and financial statements
* Must have experience with Netsuite
* Proficient with excel and google sheets
* International experience preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
* Proficiency with GitLab
* You share our [values](/handbook/values), and work in accordance with those values.
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab

## Senior Manager, Accounting

### Job Grade

The Senior Manager, Accounting is a [grade 9](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

Senior accounting managers share the same requirements as the Intermediate Accounting Manager listed above, but also carry the following:

* Manage 3rd party accounting consultants and other accounting professional contractors as needed.
* Administrator of NetSuite including chart of accounts maintenance.
* Establish and enforce proper accounting policies and principles.
* Reviews and posts journals from the GL Accountant and/or Sr. Accountant roles.
* 1st level reviewer to various balance sheet reconciliations prepared by the GL Accountant and/or Sr. Accountant roles.
* Backup to the Senior Accounting Operations Manager

## Performance Indicators

- [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition)
- [Number of material audit adjustments = 0](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)
- [Percentage of ineffective Sox Controls = 0%](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).

## Specialties

### International Reporting

The Internal Reporting Accountants plays an integral role in financial reporting, our external audit, coordination with local service providers for each of our entities, and more with a proven ability to work independently and keep up with a fast paced environment. This role helps tp create a highly efficient, world class accounting and reporting function. Additional requirements are to know your way around GAAP principles and financial statements.

### Technical Accounting
