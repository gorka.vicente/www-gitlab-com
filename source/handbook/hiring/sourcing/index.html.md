---
layout: handbook-page-toc
title: "Sourcing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose of Sourcing

The speed with which we can grow our team is dependent on the rate at which we
find qualified candidates, and the purpose of sourcing is to generate a large list of qualified potential candidates for our open roles. 

Sourcing has proved itself to be a great channel for attracting the best talent. It allows us to grow GitLab and build an even more diverse team in the most efficient way possible, while also sustaining our culture. 

## Sourced vs. Prospect

**Sourced** - a candidate who is IQA (Interested, Qualifed, Available). This candidate has shown interest and is scheduled to speak with someone (Recruiter/Hiring Manager) at GitLab.   
**Prospect** - a candidate that looks good based on the information we have available (LinkedIn, GitHub, Blogs, Conference Talks, etc.). The candidate has been approached by the Recruiting team (but we do not have any response or anything scheduled with them) or has joined our [Talent Community](https://about.gitlab.com/jobs/careers/). These should be marked as prospects in Greenhouse.  

## How to Source Candidates

"Sourcing", in general, means that you **don't** know and would **not** be able to speak about the candidate confidently in a professional sense with GitLab. At best, your sentiment might be, *"They seem qualified."* 

For anyone that you **do not** have a personal relationship with, please add them as a *Prospect* in Greenhouse. Here's how to do that: 

1. Click the **"+"** button, then **Add a Candidate** 
2. Switch to the **Prospect** tab and complete the in-take form
    * **After** receiving confirmation from a *Prospect* about moving forward in a vacancy can you change them over to a *Candidate.*

This means that such candidates **do not** qualify as *Referrals*; more information about the *Referrals* can be found on the [Referral Process](https://about.gitlab.com/handbook/hiring/referral-process/) page.

The most efficient way to source candidates for any vacancy is to search through a professional network, such as LinkedIn, AngelList, etc. for people who match the skillset and job history that you are looking for. For some positions, other networks may prove useful as well - for example, someone sourcing for a developer role could search GitLab profiles to identify promising candidates. Professional networks however make it easy to scan a person's employment history quickly and efficiently and are designed to present their best impression to potential employers. Because of this, they are a very efficient tool for finding potential candidates.

**Engagement of Candidates within the Recruiting Team** - The process of contacting the candidates are as per the following scenarios:

1. Re-contacting the candidate for the same position: If the candidate hasn't replied to the previous inmails, the other sourcer should recontact the candidate after 3 months. 
2. Re-contacting the candidate for another position: A Sourcer can reach out to the candidates if they haven't responded from the past inmails. Please consult with the other Sourcer who contacted them previously and take a call.  

**New to LinkedIn?** Use LinkedIn [Boolean hack](https://docs.google.com/spreadsheets/d/1wRGwx_GT14udxnoMOF-gP2XQaRhJA_8bEVwWwIdBio4/edit#gid=666961789)
Follow the instruction on the sheet, discuss with your recruiting partner/sourcer for the keywords. 
If you have LinkedIn Recruiter account - feel free to reach out or add into a Project to discuss with the manager or recruiting partner. 

If you don't have Recruiter account, put the leads you sourced on this [Leads sheet](https://docs.google.com/spreadsheets/d/1FT26O_qbw2yB2QTREB9c68Lw4-ojzUXFRUrF7MJwKlQ/edit#gid=1306708376), shared it with your recruiting partner so they can reach out! 

When you have identified someone as a good potential candidate, send their
profile along with any requested information to a Sourcer so they can reach out to 
the candidate and add them to Greenhouse. You can check the Sourcer alignment
[here](/handbook/hiring/recruiting-alignment/).
As an alternative, you can also ask the prospective candidate to apply directly. Kindly note that in this case, their
application won't be automatically credited to your name but you can ask the Sourcing team to change this.

If you want to reach out to a sourced candidate directly you can refer to our
[reach out templates](https://docs.google.com/presentation/d/1ySqgLoYnFUGtb7hdywav6iSb_NBPRhfIs6WZlGne6Ww/edit?usp=sharing) or
create your own message. Kindly discuss your communication strategy with your
Sourcing partner to avoid duplication and poor candidate experience.

Take a look at these [talent brand resources](/handbook/people-group/employment-branding/#talent-brand-resources) for the latest team member count, employer awards, blog posts, and more facts about working at GitLab. These resources are meant to help guide your outreach to potential candidates.

**Diversity** - In accordance to our [values](/company/culture/inclusion/#values) of diversity and inclusion, and to build teams as diverse as our users, Recruiting provides a collection of diversity sourcing tools [here](https://docs.google.com/spreadsheets/d/1Hs3UVEpgYOJgvV8Nlyb0Cl5P6_8IlAlxeLQeXz64d8Y/edit#gid=2017610662) to expand our hiring teams’ candidate pipelines. If you have any questions on how to implement these resources into your current sourcing strategy or have suggestions for new tools, please reach out to Recruiting.

## Upgrading your LinkedIn account

We're eager to provide hiring managers and all hiring team members with a **LinkedIn Recruiter** seat.

Based on LinkedIn's research, passive candidates are more likely to respond to direct outreach from a hiring manager or hiring team member than from a recruiter or sourcer. That's why we're encouraging greater adoption and use of **LinkedIn Recruiter** company-wide. A few benefits to upgrading are being able to collaboratively source candidates with other team members via *Projects* and being able to send unlimited *InMails* (*InMail* metrics is one of our KPIs).

You can request one of the following seats (we recommend requesting a **Recruiter** seat):

* **Hiring Manager:** Collaborate with your Recruiters/Sourcers on LinkedIn
* **Recruiter:** Actively source candidates on LinkedIn

To upgrade your seat, please add your GitLab email to your LinkedIn profile, then submit an [Access Request Issue](https://gitlab.com/gl-recruiting/operations/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) using the `LinkedIn Access Request` template within the [Recruiting Operations project](https://gitlab.com/gl-recruiting/operations).

To integrate your LinkedIn account with Greenhouse, please refer to the [Enabling LinkedIn Recruiter System Connect](/handbook/hiring/greenhouse/#enabling-linkedin-recruiter-system-connect) section on the [Greenhouse](/handbook/hiring/greenhouse/) page.

Please reach out to your recruiter for tips on how to improve your LinkedIn profile, write an *InMail*, and use *Projects*.

To note, we **won't** be able to reimburse any LinkedIn seats purchased at your own expense.

## Sourcing Sessions
Sourcing Session is a required step of the recruiting process for every role at GitLab. 
These sessions help us to ensure that we look for outbound talent and leverage our sourcing effort
for all the roles we’re hiring for.

Once a new role is opened, Sourcer assigned to the role will discuss with Recruiter and Hiring Manager 
who should participate in the Sourcing session and schedule it within 5 business days after the intake call.

You can find more information about Sourcing Sessions on the [Recruiting Process - Sourcer Tasks](https://about.gitlab.com/handbook/hiring/recruiting-framework/sourcer/#step-8s-source-top-talent-schedule-screens) page.

## Source-A-Thons

### Intro

As our company continues to see rapid growth, we need to aim to hire the best
talent out there and we want all GitLab team-member’s to participate with the Recruiting
team in building the greatest GitLab crew!
We ask you all to contribute by digging into LinkedIn or your favorite social
media space and add candidates to the spreadsheet.
By doing this, we are reaching out to people that are very closely aligned with
the team’s needs and finding better-suited candidates, instead of just waiting
for ad-response! (We call that, “the Post and Pray” model).

### Source-a-thon participants

* Recruiter and/or Recruiting Sourcer
* Hiring manager
* Members of the team we’re sourcing for
* All other team members willing to participate are welcome!
* Internal Source-a-thon within the Sourcing team: Sourcing team collaborates and works on the positions that are niche or time consuming. The team is divided into two, technical and non-technical. It is mandate for all the Sourcers to participate in their respective functional positions.  

### How to organize

* Hiring manager requests Recruiter/Recruiting Sourcer to organise a Source-a-thon stating the desired time and participant list
* Recruiter/Recruiting Sourcer schedules a calendar event, adds Zoom link and a ask the participants to add and save all the sourced candidates in LinkedIn projects; 
* Recruiter/Recruiting Sourcer should make sure that all invited team members have their LinkedIn account set up prior to the source-a-thon. For more information please refer to the [Upgrading your LinkedIn account](/handbook/hiring/sourcing/#upgrading-your-linkedin-account) section.

### Timing

* Source-a-thon is limited by 30 minutes
* First 5 minutes is devoted to role description - Hiring manager should share must-haves/nice-to-haves with the team and answer all the questions regarding the role
* All other time is devoted to sourcing and adding profiles into the spreadsheet
* We expect that team members can still add sourced candidates during 24 hours after the source-a-thon
* 1 day after the meeting Recruiting Sourcer will reach out to all the candidates in the spreadsheet
If any sourced candidate is added more than 1 day after the source-a-thon you should ping Recruiting Sourcer in the comments. 

### Source-a-thon targets

* Manager can set up a requirement for the minimum number of candidates each participant should add
* Manager can also require their team to attend a source-a-thon or source candidates prior to/24 hours after the meeting.

## What We Do With Potential Candidates

Save the sourcing strings for future reference to the project to help generate new strings from different perspectives.

When you source a candidate and provide their information to our sourcing team,
you can expect that they will receive an email or inmail asking them if they’re interested
in talking about an opportunity to discuss the position. The sourcer will then upload their
profile from LinkedIn to our applicant tracking system and tag their name as sourcer to 
follow their candidacy through the process.

We do try to personalize these emails or inmails as much as feasibly possible so that they
are not impersonal or "spammy," and we rely on the team sourcing to identify
relevant and qualified candidates to ensure these messages are as meaningful as
possible.

## Example: Sourcing Developers

Here's an example workflow for sourcing developer candidates:

1. In LinkedIn, search for `"ruby on rails" developer`. LinkedIn presents you
   with a list of matching profiles.
1. Based on their current company/job title, open any profiles that look
   interesting in a new tab.
1. Review the candidate's job history and skillset for positive indicators,
   like:
     * Multiple endorsements for Ruby
     * Tenure at established/strong Rails companies
     * Startup/product experience
     * ...or anything else that makes them seem likely to be a fit for our team
1. If they seem like a good fit, pass their information to the Recruiting team
by adding them as a Candidate to Greenhouse. To do so, please login to Greenhouse
and click on *Add a candidate* on the top right. Once a candidate is submitted,
please ping any Sourcer (all information about our Sourcing Team are linked on the
[team page](/company/team/)), by @ mentioning them in the
notes section in Greenhouse - this will ensure we reach out to them!
If you need any help with adding a candidate to Greenhouse - kindly reach out to
any of our Sourcers.

Following this method - and with practice scanning profiles - it's possible to
find a few dozen potential good candidates in 30 minutes or less.

## Example: Sourcing Product Designers

Here's an example workflow for sourcing Product Design candidates:

1. Before starting a search, you should spend some time defining the skills and experience you're looking for in the **ideal** Product Designer. You should look to outline the organization(s) this person *may* have worked at, the product(s) they could have worked on, any domain-specific experience they will need (e.g. monitoring, authentication, or Kubernetes), and the level this person will need to be at.
1. From here, we advise adopting a [tree ring sourcing strategy](https://business.linkedin.com/talent-solutions/blog/sourcing/2018/top-recruiters-share-their-favorite-sourcing-hacks). With this approach, you begin with a search that is based on narrow criteria, formed of all of your most desirable attributes.
1. To start your search, open LinkedIn Recruiter, and select `PROJECTS` from the top banner. Here you will need to either start a new project by naming it `Product Designer - stage group`, or open an existing project if the Recruiter has already shared one with you.
1. Once you're in the project, you'll need to open up Talent Pool.
1. Now you're on the search page. In a new project, the search field will be blank. You will see the last search run if you're in an existing project.
1. If you're in a new project, you can begin your search using the `job title` search box on the left of the interface. You can input `Product Designer` to search for anyone who has held this title in their current or past role. If you're following a tree ring strategy, you should select current from the dropdown. In an exisiting project, you can delete all the information as this will be saved by the Recruiter. You can then start your search using the `job title` field.
1.  From here, you can refine this search further using the `current company` search box. Here, you can input the names of organizations you noted at the start of this process.
1. Next, you can narrow your search further by searching for keywords that may be found within a potential candidate's Linkedin profile. The keywords you search for could be domain-specific, or keywords that could imply relevance for GitLab (e.g. Enterprise). You can narrow your search further by using a [Boolean operator](https://www.linkedin.com/help/linkedin/answer/75814/using-boolean-search-on-linkedin?lang=en) in the keyword search box within Linkedin. Authentication AND Authorization AND Enterprise could be an example keyword search you would run if looking for a Product Designer for our Manage:Access stage group.
1. Finally, you may want to narrow your search even further by searching for the number of years a candidate has been in the industry. Years of experience are an imperfect measure of ability so you should only do this if your search is returning >200 candidates.
1. In the above example, if the initial tree ring search returns too few candidates, you could broaden it by searching for (Authentication OR Authorization) AND Enterprise.
1. If you decided to search by `current` job title, you may also wish to broaden the search by selecting `current or past` in the job title search field.
1. Alternatively, you can broaden your search further by searching for alternative job titles like `UX Designer`, `Visual Designer`, or `Interaction Designer`, if you believe someone with that title will be relevant for the role you're sourcing for.

**Saving your search**

1. If you're looking to come back to the same search at a later date, you can save the search string by selecting the `save` icon. This is found next to `showing results for` at the top of the search box.

1. When you wish to come back to the saved search, you can find it by opening `search history and alerts`. Again, this is at the top of the search box on the left side of the screen.


**Some things to keep in mind when sourcing Product Designers:**

* Some Product Designers will link their portfolio directly from their Linkedin profile. You will see a link to `portfolio` on their profile, it can found below their profile header, and above their background summary.
* `Enterprise` is a powerful boolean search modifier when sourcing for GitLab. Many product design candidates who have the type of experience we look for will list enterprise software, enterprise teams, enterprise products, or enterprise applications within their online profile.  
* Occasionally a Google search for `Portfolio (insert candidate name)` can successfully bring up the candidate's online work. However, this work may be out of date, not representative of the Product Designer's current skill set, or the work of somebody else (this is especially common when a Product Designer has a commonly held name).
* Spending more than 30 seconds searching for someone's portfolio isn't advised as the Recruiter can ask potential candidates for this during any early discussions.  

