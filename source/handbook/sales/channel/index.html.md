---
layout: handbook-page-toc
title: "Channels"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Channels OKR

[OKRs](/company/okrs/)

### Channels Value 

The Channel is a critical part of our strategy moving forward as it will help us:
1. Drive growth ARR through services capacity and capability to drive customer adoption and usage of the Gitlab platform.
1. Drive and increase new customer ARR through their relationships, service engagements, and knowledge of accounts.

![GitLab Channel Value](/handbook/sales/channel/images/channel_handbook1.png)

## Channels Partner Types 

### Alliances

[Alliances](/handbook/alliances/)

### Resellers 

Primary monetization is through reselling GitLab licenses and services.
Resellers can be a service partner too (often known as a Solution Provider).

1. VAR/VAD (Value Added Reseller or Distributor): Channel services including resale, implementation, contracting, support, financing etc.
1. DMR (Direct Market Reseller): Primary business is resale of the software, often does not implement.
Value are the contracts that these partners have in place with customers.

[Learn how to become a Gitlab reseller today](/handbook/resellers/)

### Services partners

Primary monetization is through the sale of services.
This can be a one-time implementation, ongoing support or advisory, managed services, or outsourcing.
Services partners will resell Gitlab services, deliver services on behalf of Gitlab or deliver Gitlab certified services.
Services partners can be a reseller partner too (often known as a Solution Provider).

1. Global Systems Integrators have a large global workforce and can deliver on almost any customer need.
Examples: Accenture, Deloitte, TCS, Wipro
1. Regional Systems Integrators are large workforce but with single continental focus and a more limited offering of services.
Examples: CI&T, Slalom
1. Boutique Systems Integrators are very focused DevOps partners that could be deep experts on GitLab and the nuances of getting it setup and running it.
Examples - CloudReach, Flux7
1. Managed Service Providers  provide ongoing support for solutions/applications.
Examples: Rackspace

### Existing Gitlab Partner Program

[Resellers program](/resellers/program/)

### Gitlab Channels Program Updates - April 2020

1. Building a channel of enabled, DevOps & Digital Transformation focused resellers and services providers
1. Provide eStore access for SMB & Midmarket channel partners
1. Net neutral to Gitlab seller compensation
1. Incentives to identify net new customers & opportunities in existing customers
1. Incentives to attach product, operational & strategic services
1. Referral fees for non reselling services partners
1. MDF available for Demand Generation activities and events
1. Sales & SE Enablement available on demand; certifications - H2’FY21
1. Services Certifications - H2’FY21
1. Renewals incumbent protection: If a partner sells a deal and is in good standing (actively supporting the customer, etc) that partner receives first right of refusal for renewal; unless otherwise stated by the customer

## Channel Sales Enablement - Core Curriculum 

### Welcome to GitLab!

*  **What is GitLab? (Material - Video, 3 Minutes)**
   - GitLab is a single application for the entire DevOps lifecycle.
   - [Watch the video](https://www.youtube.com/watch?v=MqL6BMOySIQ).
*  **Everyone Can Contribute (Material - Video, 3 Minutes)**
   - Learn more about how we live out our Contribute value! [Watch the video](https://www.youtube.com/watch?v=V2Z1h_2gLNU).
*  **Org Chart (Material - Handbook - 2 Minutes)**
   - Check out the [org chart](https://about.gitlab.com/company/team/org-chart/) and the [Team Page](https://about.gitlab.com/company/team/)

### DevOps Technology Landscape

*  **The Software Development Lifecycle (Material - Video - 9 Minutes)**
   - This video provides a nice overview of the SDLC.
   For added context, check out the GitLab page that covers each stage in the life cycle. 
   - [YouTube - Software Development Lifecycle in 9 Minutes! ](https://www.youtube.com/watch?v=i-QyW8D3ei0)
   - [Dev Ops Lifecycle - Handbook](https://about.gitlab.com/stages-devops-lifecycle/)
   - [YouTube - How is Software Made? ](https://www.youtube.com/watch?v=UuX-GnYWNwo)
*  **GitLab SDLC Quiz (Quiz - 9 Points)**
   - Take a look at the [handbook page on the SDLC](https://about.gitlab.com/stages-devops-lifecycle/) and take this short [quiz](https://forms.gle/JPfqXxY7swi5txmf6)
*  **What is Cloud Native (Material - Handbook - 45 minutes)**
   -Taking full advantage of the power of the cloud computing model and container orchestration, cloud native is an innovative way to build and run applications. Cloud native applications are built to run in the cloud, moving the focus away from machines to the actual service.
   - [What is Cloud Native? Video](https://youtu.be/jc5cY3LoOOI)
   - [What is Cloud Native? Slides](https://docs.google.com/presentation/d/1OZVeo428BuC7xcU9lt8yI9dMzJjeiM9-Jw8YE_DSboU/edit)
   - [What is Cloud Native? Company Website](https://about.gitlab.com/cloud-native/)
*  **What is Complete DevOps? (Material - Video - 25 Minutes )**
   - In this video, we compare the traditional DevOps daisy chain to GitLab's vision for a Complete DevOps solution. Even with the adoption of DevOps, serious challenges continue to exist. Developers and operators used to be separate groups with separate tools. Complete DevOps reimagines the scope of tooling to include both developers and operations teams in one unified solution.
   - [GitLab Blog ](https://about.gitlab.com/blog/)
   - [DevOps Terms ](https://enterprisersproject.com/article/2019/8/devops-terms-10-essential-concepts)
   - [Concurrent Dev Ops](https://drive.google.com/open?id=1kRFvypIHbtIQg8lQ4nBDGWm4GvPd9gPN&authuser=0)
   - [YouTube - Why Concurrent DevOps](https://www.youtube.com/watch?v=bDTYHGEIeM0)
*  **DevSecOps Overview (Material - Sales Collateral - 25 minutes)**
   - How to integrate and automte security in the DevOps Lifecycle
   - [A Seismic Shift in Application Security](https://drive.google.com/open?id=1RKWGGmms1WBSynWNIrAJRMIOZG_ainqg&authuser=0)
   - [2019 Global Developer Report: DevSecOps](https://drive.google.com/file/d/1anFlAQoyePAQYBxGvuUFwGbPcoZftE26/view)
*  **Increase Operational Efficiencies (Material - Video - 5 Minutes)**
   - Compare the traditional DevOps Daisy Chain of disparate tools to an integrated solution for Complete DevOps. GitLab delivers a seamlessly integrated platform for developers and operators to collaborate in real-time and move ideas into production faster.
   - [YouTube - Traditional DevOps Daisy Chain](https://www.youtube.com/watch?v=YHznYB275Mg)
   - [Manage Your Toolchain Before It manages You!](https://drive.google.com/open?id=1tU4kG3sGIfb_zvECSJAr3sybPgXLs7g6&authuser=0)
   - [YouTube - You’re Living Like This? ](https://www.youtube.com/watch?v=w6X4Ha1oC6I)
*  **QUIZ: Increase Operational Efficiencies (Assignment - 25 Minutes - 6 Points)**
   - GitLab is a single application for the entire DevOps lifecycle! Please read the case study ["Manage Your Tool Chain Before it Manages You" ](https://drive.google.com/file/d/1tU4kG3sGIfb_zvECSJAr3sybPgXLs7g6/view?usp=sharing) by Forrester to answer the questions in the [Google Form Quiz.](https://forms.gle/namX7dcg1dbo5KQQ7)

### Our Customers
*  **Personas & Pain Points (Material - Video / Handbook - 13 Minutes)**
   - Introducing Personas and Pain Points. David Dulany, Sales Development Consultant, reviews the concepts of Personas and Pain Points and why they are important in setting the context for your messaging.
   - [You Tube - Introducing Personas and Pain Points](https://www.youtube.com/watch?v=-UITZi0mXeU)
   - [Handbook - Roles & Personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/)
*  **VP of App Dev (Material - Video - 14 Minutes)**
   - Watch Product Marketing Manager William Chia talk about the VP of App Dev persona.
   - [YouTube - VP App Dev](https://www.youtube.com/watch?v=58qDalA5o6Q)
*  **DevOps Director (Material - Video - 22 Minutes)**
   - [YouTube - Director of DevOps](https://www.youtube.com/watch?v=5_D4brnjwTg)
*  **Head of IT (Material - Video - 19 Minutes)**
   - [YouTube - Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
*  **Chief Architect (Material - Video - 19 Minutes)**
   - [YouTube - Chief Architect](https://www.youtube.com/watch?v=qyELotxsQzY)
*  **GitLab Digital Transformation CxO Discovery Guide (Material - 5 Minutes)**
   - [GitLab Discovery Guide](https://drive.google.com/open?id=1R6is7t4Ph3-p4tGJbDq0RhezT4j-3P0rluMV3H9-Rho&authuser=0)
*  **Customer Success Stories & Proof Points (Material - Handbook - 5 Minutes)**
   - References are an age old tenet of sales pros! Your prospective clients will definitely be impressed by the positive business outcomes of our customers. Check out our [customer case studies](https://about.gitlab.com/customers/) on GitLab value.

### Our Portfolio

* **GitLab Value Framework (Material - Sales Collateral - 35 Minutes)**
   - The GitLab [value framework](https://drive.google.com/open?id=1GV1WGyJIRuor0jxG-9ABu9ZSIBUFtPq1pqAxV9yJOvQ&authuser=0) is one of the most useful tools available for salespeople. Take a look to understand our value drivers, how to uncover customer needs, and how to articulate value and differentiation. A [framework summary](https://drive.google.com/open?id=1BawkSEbejPKx2EVgOqxtrhzHlCTGEFNzvoIF4hW3bT4&authuser=0) is also avaliable for quick reference.
*  **GitLab Features and Functionality (Material - Handbook - 30 Minutes)**
   - Check out the [feature comparison](https://about.gitlab.com/pricing/self-managed/feature-comparison/) chart to learn what's included in each package.
* **Product Tiering Quiz**
   - Take a moment to complete this short [quiz](https://docs.google.com/forms/d/e/1FAIpQLScahosIQoghrijLvRacseegt65O3SBycTIubvzJsaFn5wp2RQ/viewform)
*  **Why Sell Ultimate/Gold? (Material - Handbook - 5 Minutes)**
   - Take a look at the [handbook link](https://about.gitlab.com/pricing/ultimate/) to understand what the best plans have to offer!
*  **Pricing (Material - Handbook - 5 Minutes)**
   - Check out the chart to understand our [pricing model](https://about.gitlab.com/pricing/). For additional context take a look at the [handbook page on pricing](https://about.gitlab.com/handbook/ceo/pricing/).
*  **GitLab Direction (Material - Handbook - 5 Minutes)**
   - Our vision is to replace disparate DevOps toolchains with a single application that is pre-configured to work by default across the entire DevOps lifecycle. We aim to make it faster and easier for groups of contributors to deliver value to their users, and we achieve this by enabling: Faster cycle time, driving an improved time to innovation, Easier workflows driving increased collaboration and productivity. Our solution plays well with others, works for teams of any size and composition and for any kind of project, and provides ongoing actionable feedback for continuous improvement. You can read more about the principles that guide our prioritization process in our [product direction handbook.](https://about.gitlab.com/direction/#single-application)
*  **Product Maturity (Material - Handbook - 5 Minutes)**
   - GitLab has a broad scope and vision, and we are constantly iterating on existing and new features. Some stages and features are more mature than others. To convey the state of our feature set and be transparent, we have developed a [maturity framework](https://about.gitlab.com/direction/maturity/) for categories, application types, and stages.
*  **Use Cases (Material - Handbook - 5 Minutes)**
   - A [customer use case](https://about.gitlab.com/handbook/use-cases/) is: A customer problem or initiative that needs a solution and attracts budget. Defined In customer terms: Often aligned to industry analyst market coverage (i.e. Gartner, Forrester, etc. write reports on the topic) These are discrete problems that we believe GitLab solves and are reasons customers choose GitLab (hence which we should seek out in prospects)

### Competitive Advantages & Strategy

*  **Competitor Overview (Material - Handbook - 30 Minutes)**
   - There are a lot of [DevOps tools](https://about.gitlab.com/devops-tools/) out there.
As a single application for the entire DevOps life cycle, GitLab can remove the pain of having to choose, integrate, learn, and maintain the multitude of tools necessary for a successful DevOps tool chain.
However, GitLab does not claim to contain all the functionality of all the tools listed here.
Click on a DevOps tool to compare it to GitLab.

### Channel Sales Support

*  **Reseller Handbook (Material - Handbook - 10 Minutes)**
   - This is definitely bookmark worthy material!
The [reseller handbook](/handbook/resellers/) is your source of truth for everything sales!
* **Gitlab Reseller Webcasts**
   - Take a look at our [reseler webcasts](https://about.gitlab.com/webcast/reseller/) and sign up for a live or on demand session.
*  **GitLab Terms & Conditions (Material - Handbook - 10 Minutes)**
   - The following [terms and conditions](https://about.gitlab.com/terms/) govern all use of the GitLab.com website, or any other website owned and operated by GitLab which incorporate these terms and conditions) (the “Website”), including all content, services and support packages provided on via the Website.
The Website is offered subject to your acceptance without modification of all of the terms and conditions contained herein and all other operating rules, policies (including, without limitation, procedures that may be published from time to time on this Website by GitLab (collectively, the “Agreement”).

## Channel Sales Enablement - Advanced Curriculum

### Technical Deep Dive 

* **Auto DevOps**
   - Auto DevOps provides pre-defined CI/CD configuration which allows you to automatically detect, build, test, deploy, and monitor your applications.
Leveraging CI/CD best practices and tools, Auto DevOps aims to simplify the setup and execution of a mature & modern software development lifecycle.
[Review this document](https://docs.gitlab.com/ee/topics/autodevops/) to learn about each component of GitLab Auto DevOps.
* **GitLab API**
   - [Review this document](https://docs.gitlab.com/ee/api/README.html) to learn more about GitLab API, its capabilities and shortcomings.
* **GitLab for Agile**
   - Agile development is iterative, incremental, and lean approaches to streamline and accelerate the delivery of projects.
Ever wondered if GitLab supports Agile methodology?
If you're considering using GitLab it might not be obvious how its features correspond with Agile artifacts, so we've broken it down for you in a [blog](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/) and the [GitLab Agile Planning page](https://about.gitlab.com/solutions/agile-delivery/).
* **GitLab Runners**
   - [GitLab Runner](https://docs.gitlab.com/runner/) is the open source project that is used to run your jobs and send the results back to GitLab.
It is used in conjunction with GitLab CI, the open-source continuous integration service included with GitLab that coordinates the jobs.
Below are slides, the video presentation, and additional information about GitLab Runners.
* **GitLab High Availability (HA) and GitLab GEO**
   - [Review the GitLab Reference Architectures Overview](https://docs.gitlab.com/ee/administration/reference_architectures/), then hear Brian Wald, Solution Architect Manager, break it all down [on YouTube](https://youtu.be/fji7nvmOHNQ).

### Integrations 

* **Category Overview**
   - GitLab's vision is to be the best single application for every part of the DevOps toolchain.
However, some customers use tools other than our built-in features–and we respect those decisions.
The Integrations category was created specifically to better serve those customers.
Currently, GitLab offers [30+ project services](https://docs.gitlab.com/ee/user/project/integrations/project_services.html#project-services) that integrate with a variety of external systems.
Integrations are a high priority for GitLab, and the Integrations category was established to develop and maintain these integrations with key 3rd party systems and services.
* **Jira** 
   - GitLab Issues are a powerful tool for discussing ideas and planning and tracking work.
However, many organizations have been using Jira for these purposes and have extensive data and business processes built into it.
While you can always migrate content and process from Jira to GitLab Issues, you can also opt to continue using Jira and use it together with GitLab through our [integration.](https://docs.gitlab.com/ee/user/project/integrations/jira.html)
For a video demonstration of integration with Jira, watch [GitLab workflow with Jira issues and Jenkins pipelines.](https://youtu.be/Jn-_fyra7xQ)
* **Jenkins**
   - GitLab’s [Jenkins integration](https://docs.gitlab.com/ee/integration/jenkins.html) allows you to trigger a Jenkins build when you push code to a repository, or when a merge request is created.
Additionally, it shows the pipeline status on merge request widgets and on the project’s home page.
Videos are also available on [GitLab workflow with Jira issues and Jenkins pipelines](https://youtu.be/Jn-_fyra7xQ) and [Migrating from Jenkins to GitLab.](https://youtu.be/RlEVGOpYF5Y)
* **Github**
   - GitLab provides an integration for updating the pipeline statuses on GitHub.
This is especially useful if using GitLab for CI/CD only.
This project integration is separate from the [instance wide GitHub integration](https://docs.gitlab.com/ee/user/project/import/github.html#mirroring-and-pipeline-status-sharing) and is automatically configured on [GitHub import.](https://docs.gitlab.com/ee/integration/github.html)

#  Channel Operations

##  Channel Neutral

To incentivize working with our Channel partners, 'Channel Neutral' means that we will not reduce $ value to individual sellers even if the Channel business reduces total iACV to GitLab (via disocunts or rebates).
More information can be found on the [compensation page](/handbook/sales/commissions/#channel-neutral-compensation).

##  Definitions

**Program Compliance**
- For partners to transact, they must join the Gitlab Partner Program via the partner portal.
- Non Contracted partners may transact on a one-off basis, only with approval of channel leadership.

**PIO - Partner Initiated Opportunity**
- Any opportunity that was brought to GitLab via a Deal Registration.
- The opportunity must be new to our sales team, and can be for a new or existing customer.
- This is an upfront discount and is dependent on the partners track within the Gitlab Partner Program.

**Channel Assist Opportunity**
- Any opportunity where the partner assists our sales team to close the deal.
- This may be a customer demo, an executive introduction meeting, delivery of services, etc.
- This would be a Deal Registration, but for a GitLab sourced opportunity, so it does not qualify for PIO.
- The determination of Channel Assist is at the sales rep determination and tracked via SFDC opportunities.

**Channel Fulfill Opportunity**
- Any opportunity that was fulfilled by a partner but closed independently via the Gitlab sales team.
- The partner has only processed the order and didn’t provide any meaningful support to close the deal.

**Services Attach**
- Any partner delivered services that are provided to the end user in support of a Gitlab deployment.
- This will result in a 2.5% upfront discount from the product.
- This is stackable for up to three (3) independent services provided by the partner to a single end user.
- This will be administered as an upfront discount from the Gitlab license price on the initial sale.
- The partner may sell their own services, after the initial sale, for a rebate of 2.5% of the Net license price for a period of up to nine (9) months.
- The maximum is 7.5% discount or rebate with any combination of upfront or post sales of partner branded services.
- Services may be delivered by a non-Authorized service partner, which will be paid as a referral fee.
- Rebates and referral fees may require CRO approval.

**Services Resale**
- Any partner that resales services that will be delivered by the GitLab Professional Services team.

**Channel Neutral Comp**
- For FY21 the sales team will not absorb any channel partners standard contractual discounts.
- They will be compensated at the non-channel net value of the deal.

**Incumbency Renewal Policy**
- If a partner transacts IACV, when renewal time comes, the partner will own that renewal unless formally communicated by the customer.
- The partner will be required to be program compliant (ie in good credit standing, have provided quarterly updates on customer, review within 30 days of renewal, etc).

## Frequently Asked Questions

**Where can I find the Standard Channel Discounts for my Partners?**
- Follow [SFDC Discounting table](https://gitlab.my.salesforce.com/0694M000008xAk4)

**Where can I find more information about our current Partners processes?**
- The current Resellers Handbook can be found [Here](/handbook/resellers/)

**Whats the current Deal Registration Process?**
- The current process can be found [Here](/handbook/resellers/#deal-registration)

**How do I track the opportunity of a partner deal?**
Please fill in at least one of the following Opportunity drop downs to identify the deal as a Channel opportunity.
- **If the partner registered the deal** the `Deal Registrar`field will need to be populated in the SFDC opportunity with the appropriate Partner Account.
- **If the Partner Assisted you** (see above for definition) please populate the `Partner - Assisted` field in the SFDC opportunity with the appropriate partner account.
- **If GitLab sourced and sold the deal directly, and the partner is simply fulfilling the deal**, please populate the `Partner - Fulfillment` field in the SFDC opportunity.

**How do I check what I'm being paid on?**

**What are some examples of Channel Neutral math?**

| **Deal Calculation** | **Direct Deal** | **Channel 1 (Neutral)** | **Channel 2 (Add’l Disc)** | **Channel 3 (Split Disc)** |
|----- | ----- | ------ | ------ | ------|
| List Price | $100,000 |  $100,000 | $100,000  | $100,000 |
| Channel Discount | 0% | 20% | 20% | 20% |
| Additional Rep Discount | 5% | 0% | +5% | +2.5% |
| **Total Discount** | **5%** | **20%** | **25%** | **22.5%** |
| IACV (Quota Relief) | $95,000 |  $80,000 | $75,000  | $77,500 |
| Channel Neutral (No Quota Relief @ BCR)  | +$0 |  +$20,000 | +$20,000  | +$20,000 |
| **Commissionable Amount** | **$95,000** |  **$100,000** | **$95,000**  | **$97,500** |
| Base Commission Rate (BCR) | % 8 |  % 8 | % 8  | % 8 |
| **Commission Payout** | **$7,600** | **$8,000** | **$7,600**  | **$7,800** |


**How do I get Channel deals/discount approvals?**
Follow [standard approval process](/handbook/business-ops/order-processing/#step-5---submitting-a-quote-for-discount-and-terms-approval) in SFDC 
    
**How does the Amazon Process work?**
Follow [Amazon Web Services (AWS) Private Offer Transactions](/handbook/business-ops/order-processing/#amazon-web-services-aws-private-offer-transactions) handbook

**I need HELP!  How to do I reach out to the experts?**

The quickest way to get help is by using the following Slack channels:
- #channel-sales
- #channel-ops
- #alliances
