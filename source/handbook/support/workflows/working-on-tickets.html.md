---
layout: handbook-page-toc
title: Working on Tickets
category: Handling tickets
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is the "Working on Tickets" workflow?

The [core responsibility of GitLab Support Engineers](/handbook/support/support-engineer-responsibilities.html) is to resolve customer problems by solving support tickets on a daily basis. You accomplish this using the "Working on Tickets" workflow. The focus is **being responsible for the continuous progress toward resolution of a specific set of tickets**. What does this look like?

It starts with assigning a ticket to yourself at the moment when you make the first public comment on it. From that point forward you should be thinking about how you can keep that ticket moving toward a resolution that's acceptable to the customer. Don't worry - you're not responsible for creating the solution, only for making the solution happen. **You're the leader, not the sole contributor.** If you can resolve the ticket independently, great! If not, ask others to help by working with you (*e.g.,* create a thread in Slack, arrange a pairing session). If you're not able to find someone to work with you, please let your manager know so that they can help with next steps.

Benefits of working on tickets assigned to yourself:
1.  You won't have to read through a series of updates from multiple support engineers, and the customer, to understand the ticket history, current status, or plan of action. In fact, we encourage you to include a brief description of those items - history (what's happened so far), current status, next steps - in most of your updates to the customer so that your last update always tells you what you need to know.
1.  With one person in charge of each ticket, you can take a consistent approach to resolving the problem, you can explain why and what you're doing, and you won't repeat or contradict an action taken by somebody else on the ticket. This is a much better experience for the customer.
1.  Knowing that you're in charge of a ticket gives you freedom to set and manage customer expectations at each step along the way, so the customer will know what's going on and who's working on their behalf. Again, this is a customer experience enhancement.

When you're "Working on Tickets", you're driving achievement of our KPI of [Support Satisfaction](/handbook/support/#support-satisfaction-ssat) by helping to resolve tickets as quickly and effectively as possible.

### The Process in Action

Here's what to do when you're actively working on tickets in Zendesk. Divide your efforts between:

1.  Take **next** steps on tickets already in your [My Assigned Tickets](https://gitlab.zendesk.com/agent/filters/360062369834) view.
*  Identify and take **first** steps on new tickets in the main queue (`Self-Managed MY_REGION & All Regions - Needs assignee` view).

The basic workflow looks like this:
1. Start your work at the top of your own queue
1. Determine whether you can identify any next steps to make progress
   1. If you can, then take those steps, including updating the customer
   1. If not, then link the ticket in Slack and ask for help from your teammates
1. If it's been an hour or more since you checked the main queue (or your own queue is up-to-date or empty), and you have the capacity to take on another ticket, then:
   1. If the ticket at the top of the main queue has an SLA that will expire in an hour or less, assign that ticket to yourself 
1. If you've asked in Slack for help on a ticket, and nobody has stepped up to help, and you feel you can't wait any longer for help, then:
   1. `@mention` your manager in Slack in the thread where you requested help, and ask them what the next steps should be
1. If there are tickets left in your queue to work on, then go back to step 1
1. Go back to `Finding a New Ticket`

See the [Working on Tickets Flowchart](#working-on-tickets-flowchart) for a visual representation.

<!-- ![Working on Tickets process flowchart](assets/working-on-tickets-workflow.png) -->

### Keep In Mind:

1. Teamwork is really important with this workflow. Be aware of what's happening across the board and jump in! Pairing with someone is a great way to work out complex issues.
1. When you send the customer an update, the normal action is to set the ticket status to `Pending`. We are now waiting for a reply from the customer and there is no SLA clock counting.
1. Occasionally you will send a customer a reply letting them know that you're going to follow up with further information. In this case you should set the ticket status to `Open` or `On-hold`. When this happens ZenDesk still removes the SLA. To help ensure we don't forget to follow up, a trigger automatically assigns the ticket to you (if it's not already assigned to someone else) so that you can see it in your queue.
1. For all tickets in which the customer is waiting on a response from Support, **always provide an update** on its status and work that has been done. Aim to do this daily, and definitely no less than every four days (the length of the On-hold period).

### FAQ About Working on Tickets workflow

1. **How many tickets should I have assigned to me?** There's no specific number. Aim to take as many tickets as you can while being sure that you can give good attention to each of them daily (unless they’re on hold). You might expect that to be somewhere between three and ten open, pending or on-hold tickets in your [My Assigned Tickets](https://gitlab.zendesk.com/agent/filters/360062369834) view.
1. **A ticket is assigned to someone else - can I work on it?** Absolutely! Support Engineers should [balance their day](/handbook/support/support-engineer-responsibilities.html#2-help-meet-our-service-level-objectives-for-ticket-reply-times-daily) between being 'proactive' (working on your assigned tickets) and 'reactive' (helping prevent tickets from breaching their SLAs). Tickets breaching soon may be assigned to a team member in a different region. When replying to these tickets, be sure that your next steps align with the action plan that the assignee has described on their replies or ticket summary.
1. **I'm going to be absent, what do I do with my assigned tickets?** If you'll be absent for only a day or two, please ask your customers whether they want to wait for your return or work with others in your absence. For those who will wait, place their tickets on hold. For longer absences, please inform your customers that your tickets will be reassigned due to your upcoming absence. Then, find someone willing to take over each ticket, and review the tickets with them before reassigning. If you can't find someone to assign your ticket to, you can 'unassign' by assigning it to a 'group' (e.g. 'Support EMEA', 'Support APAC', 'Support AMER'). It doesn't matter which group - the ticket will still be visible for all Support Engineers. Be sure in this situation to create a very clear and thorough ticket summary so that the next engineer can come up to speed very quickly.
1. **A ticket has diverged into more than one problem. What do I do?** It's recommended to keep tickets focused on a single problem that's clearly described by the ticket Title. If the customer asks about another problem, you are encouraged to create a new ticket on behalf the customer that focuses on a single issue and keep the original ticket on the original issue. This helps reduce time to resolution and makes it easier for us to focus on fixing the problem at hand.
1. **Can I reassign a ticket to someone else?** There are some situations in which you should look to reassign a ticket:
   1. If you'll be absent, see the FAQ above about reassigning tickets when you'll be absent.
   1. If you've determined that specific expertise outside your own is required to resolve the ticket, your first choice should be to pair with an expert so that you can get the ticket resolved and learn in the process. But if pairing is not reasonable, go ahead and reassign the ticket after discussing it with the other support engineer.  Be sure to send a message to the customer informing them that you’ve asked another support engineer with relevant expertise to take over the ticket, and that you’ve reviewed the ticket with that engineer.
   1. If you've become overloaded with tickets, feel free to rebalance your load by finding one or more teammates to take over some of your tickets. Be sure you discuss each ticket before reassigning it so that the other support engineers don't have to start from scratch. Then inform the customer that you’ve asked another support engineer to take over the ticket due to time constraints.

## Working on Tickets Flowchart

```mermaid
graph TD
A([My Tickets]) --> B
B[Open the 'My Assigned Tickets View' in Zendesk] --> B1
B1{Are there tickets left in <BR> your view that need work?} -->|Yes| C
B1 -->|No| NT2
C[Select top ticket from queue] --> D
D{Can you make progress and <BR> take the next steps?}
D -->|Yes| E[Update the customer<BR>with the next steps]
D -->|No| F[Link the ticket in<BR>Slack and ask <BR> teammates for help]
E & F --> NT0

NT0([Finding a New Ticket]) --> NT1
NT1{Have you checked <BR> the main queue <BR> in the last hour?} -->|Yes| AH0
NT1 -->|No| NT2
NT2{Do you have capacity <BR> for another ticket?} -->|No| AH0
NT2 -->|Yes| NT3
NT3{Is there a ticket in the <BR> main queue expiring <BR> within an hour?} -->|No| AH0
NT3 -->|Yes| NT4
NT4([Assign the ticket to yourself]) --> B1

AH0([Check on Help Requests]) --> AH1
AH1{Is there a ticket <BR> on which you need <BR> help but nobody's <BR> volunteered?} -->|Yes| AH2
AH1 -->|No| B1
AH2["@"mention your manager in <BR> Slack in the thread <BR> where you requested help. <BR> Ask them for next steps.] --> B1
```
## Tips for working on tickets (all roles)

### I'm working on a ticket, how do I let the team know?

1. If the ticket is unassigned, then:
   1. Assign the ticket to yourself:
      1. This takes it out of the 'Needs assignee' view so other engineers won't waste
         time on it
      1. *NOTE: The SLA clock continues to run!*
   1. Work on the ticket and aim to get a helpful reply out to the customer. Work with the team if needed as described above.
   1. If you're not able to get a reply out to the customer even after seeking help, it's OK to unassign the ticket as described above, though this should be a last resort. Please add an Internal Note summarizing anything you discovered during your research.
   This helps the next person who picks up the ticket.
1. If the ticket is already assigned to someone else and you're stepping in to
   help them, then:
   1. Add yourself to the **CCs**
   1. Enter a short internal note stating "assisting"
   1. Proceed to write and send your reply to the customer, or to pair with the
      assignee to work on the ticket

### What if someone is working on a ticket that I’d like to work on?

If another engineer is looking at a ticket that you’re interested in working on:

1. Contact them in Slack to confirm whether they're actively working the ticket.
1. If they are, ask to pair on the ticket, or share with them any information
   and questions that you have.
1. If they don’t respond in Slack, go ahead and work on the ticket - [Bias for Action](/handbook/values/#bias-for-action) is part of GitLab's core Value, Results.

### A ticket is close to breaching SLA; I'm working on it, but I need more time for my full reply.

1. Please send the customer a short message to update them on the action plan:
   what you're doing, what progress you've made, and what's left to do. Let
   them know when you expect to have next steps for them to take. Doing this
   will not only prevent an SLA breach, it will very likely be well-received by
   the customer. Remember, you don't need to have a full answer in order for
   your message to be useful.
1. When sending the short public reply, set the ticket status to `On-hold` or `Open`. `On-hold` is useful when waiting for information from another team. `Open` is useful when you want to keep the ticket visible to the rest of the Support team. (See below for more details on choosing a ticket status.)
1. When you take the above action, **keep your commitment and get back to the
   customer on time and with a full reply including next steps**. Keep in mind
   that you've stopped the SLA clock, so it's up to you to respond on time. **Do
   not take this action if you are not planning to be the one to follow up.**

### Understanding Ticket Status

Each ticket in Zendesk has a [status](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets) that tells you what state it's currently in. They are as follows.

|  **Status** | **Meaning** | **Notes** |
| --- | --- | --- |
|  New | The ticket has just been opened and has had no replies. |  |
|  Open | The ticket has had one or more replies, and the customer is waiting on GitLab Support to provide the next reply. |  |
|  Pending | The ticket has been replied to and is waiting on the customer to provide additional information. | After 3 days in "Pending", a customer will receive a further response reminding them that the ticket exists. If there are no responses after a total of 7 days, the ticket will be moved to Solved. |
|  On-Hold | GitLab support is working on the ticket and may be waiting for information from another team | Placing a ticket on-hold will assign it to the engineer. After four days the ticket will move back to open status, requiring an update to the customer. On-hold is transparent to the customer (they see the status as 'Open') so there is no need to inform the customer that the ticket is being put on-hold. It's the engineer's responsibility to ensure timely replies or set the ticket back to 'Open' if they are no longer working on it. Setting a ticket to 'on-hold' while working on the ticket can be useful as it takes it out of the main queue, thus saving other engineers from wasting time reading the ticket. |
|  Solved | The ticket has been solved | A customer who replies to a Solved ticket will re-open it. A Solved ticket will transition to 'Closed' after 4 days. |
|  Closed | The ticket is archived | A customer who replies to a Closed ticket will open a new ticket with a note that relates the new case to the closed ticket. |

### Handling Large Files

Zendesk has a [fixed maximum attachment size](https://support.zendesk.com/hc/en-us/articles/235860287-What-is-the-maximum-attachment-size-I-can-include-in-ticket-comments-) of 20MB per file. If you need a customer to share a larger file than this, then see [Provide Large Files to GitLab Support](/support/providing-large-files.html) for information on how to do so.

### Filling Out Ticket Fields

Depending on the queue you are working on and the form the ticket belongs to, you might need to fill out some ticket fields manually. Those fields help us capture important data that will help us improve the customer experience. As a high percentage of our tickets are solved/closed automatically through our workflows, it is important to make sure that before you submit your response to a ticket, you check that all required (*) fields and relevant non-required fields have been filled out.

### Copying contents of Slack threads to internal notes

When using Slack to work with others or communicate internally regarding a support ticket, please bear in mind [our chat retention policy](https://about.gitlab.com/handbook/communication/#slack) and the [Communication Guidelines (esp. 9.)](https://about.gitlab.com/handbook/communication/#general-guidelines). It's best for future searches in Zendesk to copy relevant advice, notes, ideas, etc. from Slack to an internal note in Zendesk.

### Understanding SLAs

Our SLA workflow relies on end-users who submit tickets belonging to an organization and that organization having a GitLab Plan. Organization information is automatically added to Zendesk via a Salesforce Integration. We sync all records with Account Type equal to `Customer` from Salesforce to Zendesk. The information we get from Salesforce includes but is not limited to: Account Owner, Technical Account Manager, GitLab Plan and Salesforce ID. Organizations should never be created manually in Zendesk as that can cause our sync to be ineffective.  If you think an Account in Salesforce doesn't have an equivalent Organization in Zendesk, please let the Support Operations Specialist know so a manual sync can be run.

SLAs are set as Business Rules within Zendesk. For more information, please refer to the specific [Zendesk](/handbook/support/workflows/zendesk_admin.html) page.

We have a Slack integration that notifies us of impending breaches:
1.  Self-managed - if a Premium or Ultimate ticket will breach within an hour
1.  GitLab.com - if a Silver or Gold ticket will breach within 2 hours

If you see an SLA notification in Slack, start a thread and consider this a _small emergency_. If you need help, draw the attention of other support engineers by tagging them, and work to move the ticket forward.

If a customer's reply is the last one in the ticket, do not set it to any status silently (except for Solved), because the breach clock will continue to run and the ticket may breach silently. Instead, send a confirmation, greeting, or other message, while also changing the status.

### GitLab.com Views

Support Engineers who are currently focused on helping GitLab.com customers should work on tickets in the [GitLab.com views](/handbook/support/workflows/zendesk-overview.html#views).

### Self-Managed Views

Support Engineers who are currently focused on helping self-managed customers should work on tickets in the [Self-Managed views](/handbook/support/workflows/zendesk-overview.html#views).

### Non-Support Views

1. Security
1. Upgrades, Renewals & AR (refunds)


### Using On-Hold Status

You should use the On-hold status when it is necessary to do some internal work, e.g. reproduce a complex bug, discuss something with developers or wait for a session scheduled with a customer. When setting the status to On-hold it will be automatically assigned to you by the trigger [`Automatically assign on-hold ticket to an agent who put it to the on-hold status`](https://gitlab.zendesk.com/agent/admin/triggers/360033242313).

If you think that it should be assigned to someone else (e.g. session is scheduled for another engineer), feel free to re-assign it. Tickets without assignee will be automatically reopened by the trigger
[`Automatically reopen on-hold tickets without assignee`](https://gitlab.zendesk.com/agent/admin/triggers/360028981853). Tickets in on-hold status _with_ an assignee will be automatically reopened in 4 days by the automation [`Reopen on-hold tickets after 4 days`](https://gitlab.zendesk.com/agent/admin/automations/360028978393).

If a customer's reply is the last one in the ticket, do not set it to the On-hold status silently due to the same reasons as stated above in the
[Zendesk SLAs and Breach Alerts](#zendesk-slas-and-breach-alerts)). Instead, reply to the ticket while also changing the status.

### Merging Tickets

If you're merging two customer tickets that are related and it's not 100% obvious to the customer, be sure to send a message letting them know why you're merging them. If you don't, it often causes confusion and they open follow ups asking why it was closed without comment.

Additionally, when [Merging Tickets](https://support.zendesk.com/hc/en-us/articles/203690916-Merging-tickets), leave `Requester can see this comment` **unchecked** in the ticket that's being merged into (the second ticket from the top) in order to maintain the SLA. If the merge comment is made public, Zendesk considers it a response and removes the SLA.

**NOTE:** Any ticket merge is final -- there is no option to undo it.

### Removing Information From Tickets

We ask customers to send us logs and other files that are crucial in helping us solve the problems they are experiencing. If a customer requests deletion of information shared in a support ticket, if we suspect sensitive information was accidentally shared, ask a Support manager to delete the files using the [`Ticket Redaction`](https://www.zendesk.com/apps/support/ticket-redaction/) app. Only Zendesk administrators have access to this app.

To delete text or attachments from a ticket:

1. Go to the ticket in question and on the right hand nav bar, scroll down until you are able to locate the Ticket Redaction app.
1. In the text box, enter a string of text or source image URL you wish to redact.
1. If you wish to remove an attachment, you can click on the `Redact Attachment` button and choose the attachment you would like to remove.

# How to succeed at working on tickets

## 1. Weekly ticket review

### Aim

1. Make your work visible
1. Develop consistent high quality ticket work across the team

### How to do it

1. Review at least two tickets each week in your 1:1 call with your manager
1. Tickets can be selected by you or your manager
1. They can be tickets with work you're proud of or where you would like feedback
1. Your manager will give you feedback on:
     1. Technical quality
     1. Communication quality
     1. Collaboration quality (where appropriate, did you work with other Support Team members, managers, other GitLab team members or directly with the customer on a call)

### Context

1. This is currently an informal process between you and your manager.
1. It is not part of a formal performance review.
1. We're investigating a more comprehensive [ticket retrospective process](https://gitlab.com/groups/gitlab-com/support/-/epics/23).

## 2. Public replies on your assigned tickets

### Aim

Balance your focus between 'proactive' work to continue the progress on your assigned tickets and 'reactive' work to assist in making progress and preventing SLA breaches on other tickets.

### How to do it

1. Aim for around 50% to 60% of public replies to be on tickets that are assigned to you.

[TODO: A report will be made available to Support Engineers so you can easily see how you are doing.]

## 3. Meet the ticket baseline

### Aim

Help encourage an even distribution of 'volume' of ticket work amongst the team. 

### How to do it

1. Focus on solving customer problems via tickets as the core part of your work.

That's it! For most people that's all you need to do. Once you've completed onboarding and have been helping out with tickets for two months or more, it's useful to gauge your contribution compared to the rest of the team.

### Context

Each week we publish the 'mean average per Support Engineer' for solved tickets, public replies and internal notes in the Support Week in Review. 

We produce separate reports for [self-managed](https://gitlab.zendesk.com/explore#/pivot-table/connection/10438872/query/31477512) and [.com](https://gitlab.zendesk.com/explore#/pivot-table/connection/10438872/query/31478192) tickets in recognition that the volume of tickets for the two roles is different.

We establish a **dynamic baseline** that is 0.85 of the mean average* for each metric. \
(*value is chosen based on the [threshold for the lower quartile](https://en.wikipedia.org/wiki/Quartile)).

Here's an example week for folks working on self-managed tickets:

<table>
  <tr>
   <td><strong>Number of self-managed Support Engineers</strong>
   </td>
   <td><p style="text-align: right">
<strong>50</strong></p>

   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td><strong>Solved</strong>
   </td>
   <td><strong>Public Comments</strong>
   </td>
   <td><strong>Internal notes</strong>
   </td>
  </tr>
  <tr>
   <td><strong>Totals for last week</strong>
   </td>
   <td><p style="text-align: right">
300</p>

   </td>
   <td><p style="text-align: right">
1000</p>

   </td>
   <td><p style="text-align: right">
500</p>

   </td>
  </tr>
  <tr>
   <td><strong>Average per agent</strong>
   </td>
   <td><p style="text-align: right">
6</p>

   </td>
   <td><p style="text-align: right">
20</p>

   </td>
   <td><p style="text-align: right">
10</p>

   </td>
  </tr>
  <tr>
   <td><strong>Baseline (0.85 of avg)</strong>
   </td>
   <td><p style="text-align: right">
<strong>5.1</strong></p>

   </td>
   <td><p style="text-align: right">
<strong>17</strong></p>

   </td>
   <td><p style="text-align: right">
<strong>8.5</strong></p>

   </td>
  </tr>
</table>

1. Support Engineers that have completed onboarding are expected to achieve the baseline as a minimum. Don't worry in a given week if you don't meet the numbers. The baseline is a guide to help balance team contributions - it is not a stick to penalize anyone.
1. There are good reasons why you might not achieve the baseline on a given week (e.g. vacation, illness, working on other projects, carrying out interviews) and you should keep your manager informed when you're not able to focus on resolving customer problems by contributing to support tickets.
1. If you don't meet the baseline you should discuss the reasons with your manager. If they're not satisfied that there are good reasons for not meeting the baseline, they will ask you to focus more on solving customer problems on Support tickets so that you meet the baseline in future weeks. This helps balance contributions across the global team.
1. We believe in quality, not quantity - more is not automatically better.
1. We recognize that the three metrics selected are not a complete indication of 'good work' on their own.
1. We recognize that the metrics for 'public replies' and 'internal notes' can be 'gamed' by making small updates. We trust the team to do the right thing for the customer and GitLab. This is why the ticket baseline is the last part of 'how to succeed' in this area. It is more important to make quality updates and balance your focus between 'proactive' work on your assigned tickets and 'reactive' work to prevent SLA breaches.
1. There is no automated process to report on people that don't meet the baseline. The baseline is there to help you gauge your contribution and as a conversation starter between you and your manager.
