---
layout: markdown_page
title: "Third Party Vendor Security Review process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Process Overview and Guide for Procurement Requests

Security Compliance performs annual security reviews of vendors for corporate tools and services that may process non-public GitLab data. As part of [New Vendor Evaluations](/handbook/finance/procurement/#1-new-vendor-evaluation) and [Vendor Contract Renewals](/handbook/finance/procurement/#2-existing-vendor-negotiation-for-renewals-and-true-ups), a security review is [required](/handbook/finance/procurement/#security-engagement-for-vendor-contracts) if processing, storing, and/or transmitting non-public data, which is all data 
[classified](/handbook/engineering/security/data-classification-policy.html) 
as Yellow or higher. This extra scrutiny is due to the fact that GitLab no longer controls our data when it is processed, transmitted, or stored by a third party.

Security Compliance identifies potential risks to GitLab with the introduction of the third party based on the vendor [security reports](#Vendor Documentation Decision Tree) and engagement details, and identifies any compensating controls that may be available. 
The output of the review is not meant to be a recommended "Yes" or "No" from Security Complaince. Instead, all risks identified are shared with the relevant stakeholders to enable them to make the best business decision. The goal of the vendor security review is to reduce the likelihood that GitLab data will be exposed or mishandled by raising awareness and providing consultancy.
Security management approval is required for vendors with major risks and the business owner is subject to the [risk acceptance process](#Accepting risks identified during the procurement process).

This vendor security review process is not meant to slow down the procurement process, but a certain amount of friction to this process is inevitable. Refer to the decision tree below to determine whether a security review is required. 

```mermaid
graph TD;
    A[New Vendor or Contract renewal] --> B{"Will this vendor be used for<br>marketing purposes such as <br>events, programs, sponsorships<br>hotels, content, or creative<br>and professional services?"};
    B --> |Yes| C["No Security Review is required <br>if non-public data will not be stored or <br>processed by the vendor as part of this contract <br>(i.e. any information not available in our <br>handbook). Right click this box and <br>open in a new tab to navigate to the <br>appropriate vendor contract approval<br>issue template."]
    B --> |No| D[A Security Review may be required.]
    D --> E{"Will non-public data<br>be stored or processed<br>by the vendor as part of<br>this contract? (i.e. any<br>information not available<br>in our handbook)"}
    E --> |No| F[A Security Review is<br>not required.]
    E --> |Yes| G["A Security Review is required.<br>Determine the type of data that<br>will be stored or processed per<br>our data classification policy<br>(red, orange, or yellow data).<br>Right click this box and open<br>in a new tab to navigate to<br>our data classification policy."]
    G --> H{Is an NDA already in place?}
    H --> |No| I[An NDA is required for any<br>vendor contract involving<br>non-public data. Right click this<br>box and open in a new tab<br>to navigate to the handbook<br>process for creating an NDA.]
    H --> |Yes| J[Complete the vendor contract approval<br>issue template by right clicking this box<br>and opening in a new tab.]
    J --> K[Reach out to the vendor contact and<br>request Security Documentation that<br>will be required for a Security Review.<br>Right click this box and open in a<br>new tab to navigate to an email<br>template that can be used.]
    click C "https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts_marketing_events" "Vendor Contracts Marketing Events Issue"
    click G "https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html#data-classification-levels" "Data Classification Policy"
    click I "https://about.gitlab.com/handbook/finance/procurement/#1-new-vendor-evaluation" "Creating an NDA"
    click J "https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts" "Vendor Contract Approval Issue"
    click K "https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html#email-template-for-requesting-security-documentation" "Security Documentation email template"


  style A fill:#e6e6e6
  style B fill:#e6e6e6
  style C fill:#a2f2a9, stroke:#0000ff, stroke-width:2px
  style D fill:#e6e6e6
  style E fill:#e6e6e6
  style F fill:#a2f2a9, stroke:#0000ff, stroke-width:2px
  style G fill:#e6e6e6, stroke:#0000ff, stroke-width:2px
  style H fill:#e6e6e6
  style I fill:#e6e6e6, stroke:#0000ff, stroke-width:2px
  style J fill:#e6e6e6, stroke:#0000ff, stroke-width:2px
  style K fill:#e6e6e6, stroke:#0000ff, stroke-width:2px
```

#### Email template for requesting security documentation
You can help expedite this process by sending the following message to the vendor:

> Because the use of your tool or services requires the protection of GitLab data and/or there is a reliance on your tool or service to support various business/financial processes at GitLab, our Security Compliance team will need some additional information to ensure that controls in your environment provide reasonable assurance that processes we rely on are operating effectively and that our data is appropraitely secured. If you have gone through an independent certification for a SOC1 Type 2 or SOC2 Type 2, and have undergone a recent PenTest, please provide a copy of all these reports or summary of results. Additionally, if there are bridge letters available to supplement your SOC reports, please provide these so that our Security Compliance team can determine that there have been no major changes to controls over your environment between certifications. If you have not gone through these certification processes, please complete the GitLab Information Security Questionnaire and answer only the applicable questions. We appreciate your partnership in helping GitLab complete its due diligence requirements.

#### Vendor Documentation Decision Tree
In the event that the vendor cannot provide the above information, the following shows what documentation is required in order to complete this vendor security review process:

Does the vendor have an external audit report that shows how controls were tested and those testing results? (e.g. SOC1 Type 2 and SOC2 Type 2 reports, ISO 27001 audit reports, etc.)
   * `If yes`, does the scope of that report cover the services that GitLab will be using?
      * `If yes`, please request these reports and share with the security compliance team for review. Also, please request a [penetration testing report](#recent-penetration-testing-report)
        * `If no`, does the vendor have an external audit report that does cover these services?
         * `If yes`, please request this report and share with the security compliance team for review. Also, please request a [penetration testing report](#recent-penetration-testing-report)
         * `If no`, please continue through this decision tree
   * `If no`, Does the vendor have a point in time assessment of the design of controls (e.g. CSA CAIQ self-assessment, SOC1 Type 1 and SOC2 Type 1, etc.)?
      * `If yes`, please request this report and share with the security compliance team for review. Also, please request a [penetration testing report](#recent-penetration-testing-report)
      * `If no`, please continue through this decision tree

Is the vendor willing to fill out GitLab's information security questionnaire so we can assess the maturity of their information security program?
   * `If yes`, please share the [GitLab Information Security Questionnaire](https://docs.google.com/spreadsheets/d/1isL6fkYX45Wb0BewozUPROnGgirmonTENFuBGapS_Xk/edit#gid=1307424571) with the vendor. Also, please request a [penetration testing report](#recent-penetration-testing-report)
      * **Note: It can take vendors some time to complete these questionnaires so the earlier we start this process the better **
   * `If no`, please complete a [risk exception](/handbook/engineering/security/security-assurance/security-compliance/risk-management.html#risk-exceptions) so we can document the fact that we are proposing to move forward with a vendor for which we have no insight into the maturity of their security program.

##### Recent Penetration Testing Report
Penetration testing reports have a very different scope and goal than audits of an information security program audit, but they are very useful when assessing the maturity of a vendor's information security program. The fact that a vendor has completed such a test indicates that they are operating a mature security program and are reducing the likelihood of a vulnerability existing in architecture or software. For this reason we request a copy of a recent penetration testing report as a part of all documentation requests as well as a current state of the remediations associated with any findings in that report. If the vendor has policies that don't permit sharing of the report, a summary of the findings and their status confirmed by the vendor is acceptable. In cases where only PCI Atetstation of Compliance is available for sharing, we will accept that for our records.

##### What if a vendor is providing a security whitepaper instead?
Unfortunately, security whitepapers rarely give us all of the information we need. When a company generates these whitepapers they are always from the perspective of everything the vendor feels like they are doing well, but won't have any information about what controls are not in place or not yet mature. For this reason we won't accept whitepapers in place of audit reports or the GitLab Information Security Questionnaire, but the vendor can absolutely attach the whitepaper and reference any information in there when completing our questionnaire if that makes the process easier for the vendor.
 
#### Third Party Vendor Hardening Guide
This helpful [guide](/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-hardening-guide.html) is for business owners and third parties who wish to know more about complying with GitLab's best practices for sensitive data. As specifications and requirements are identified by stakeholders, those will be added to this page as security guidance for sourcing.
<br>
<br>
<br>
## Vendor Security Review Process

#### Finance Issue

*  When a procurement issue is opened for new services, there is a section for `Data Processing` that must be completed by the business owner requesting the service and validated by the Security Compliance DRI. This will determine the secuity review level needed which must be followed in tandem with the Contract and Approval Workflow. 

*  The instructions ask to request security reports and certifications including SOC1, SOC2 Type 2, and the latest Penetration test. 

*  The label "SecurityReview::NotStarted" is applied by the template and the "SecurityReview::" label workflow is managed thereafter by the DRI to communicate the phases of the review.

*  If the review is blocked due to missing information from the business owner or vendor reports, the blocked label set "SecurityReview_" is used to indicate the reason for the delay. 

*  Once resolved the label can then be updated back to the appropriate phase of the "SecurityReview::" workflow.

#### Third Party Vendor Security Management Issue

*  Security review issues are automatically created within the [Third Party Vendor Security Management repo](https://gitlab.com/gitlab-com/gl-security/compliance/third-party-vendor-security-management) upon new procurement requests for [professional services](general_vendor_contract_request.md) and [software](software_vendor_contract_request.md) purchases. The DRI is also tagged within the procurement issue by the bot for visibility.

*  Security review issues are opened for [field marketing requests](/gitlab-com/finance/-/blob/master/.gitlab/issue_templates/vendor_contracts_marketing_events.md) which by default only handle public data, so these issues serve as a record since there is no security review to carry out. They also help with metrics.

*  If identified data is Yellow or above, the DRI will engage for the reports and/or questionnaire if missing in the procurement issue as part of the security intake steps.

*  Information is requested from the vendor as needed to complete the review of the security management practices and technical security posture. 

### Completing the Review

*  Reports and questionnaires returned by the vendor are [reviewed for noted exceptions and gaps](/gitlab-com/gl-security/compliance/compliance/blob/master/runbooks/Vendor_Security_Report_Review.md).

*  Exceptions are recorded according to the instructions within the template and shared in the original procurement issue. 

*  The DRI then selects the appropriate `Security Approval` in Step 5 of the procurement issue. If management approval is needed, the additional steps are followed to tag for review otherwise completes the review by updating the label.

*  Observations that pose a potential risk to GitLab and that does not have a mitigation plan are tracked as part of ongoing vendor risk management processes using the [VRM label](/gitlab-com/gl-security/compliance/third-party-vendor-security-management/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=VRM_for_tracking) for follow up and annual review.

## Criteria
Security reviews are largely operating on a guidance basis where Security Compliance provides recommendations for risks identified as having the probability of impacting GitLab. This is based on the vendor's score which is determined by the
data types processed, the intergration and access levels and any reported deficiencies from the security reports or questionnaire. The DRI consults with the [STORM scoring methodology](/handbook/engineering/security/security-assurance/security-compliance/operational-risk-management-methodology.html#risk-factors-and-risk-scoring) to identify the score and treatment plan.

## SLA
Security Compliance commits to a 3-business day SLA for completion when not waiting on input from the vendor. The scoped lables will be used to audit SLAs.

## Accepting risks identified during the procurement process
If the result of a vendor security review is either:
1. We were not able to get any security documentation from the vendor; or
1. We identified 1 or more findings that indicate the vendor's security program is not appropriately mature or has known flaws
then our findings will be presented to the Senior Director of Security for a final determination on whether or not that vendor can be approved from a security perspective.

If a GitLab team wants to move forward with a vendor without security approval, then a [risk exception](/handbook/engineering/security/security-assurance/security-compliance/risk-management.html#risk-exceptions) is required.

The purpose of this risk exception is to ensure that when GitLab leadership is approving of these risks, they do so with full context and insight into the tangible dangers presented by that risk to our team-members and customers. The output of this process also gives us a record to track organizational risk which is required in external audits.
