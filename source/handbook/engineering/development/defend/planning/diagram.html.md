---
layout: handbook-page-toc
title: Defend Product Development Flow
---

# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Product Development Flow Diagram

This flow is a visual representation of the [Product Development
Flow](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-1-validation-backlog)
handbook page. It does not include many of the detailed steps and interactions occurring within
phases nor the transitions of various workflows labels.

**Legend**

Box colours represent the type of issue:

```mermaid
%% editing these is a lot easier if you use the live editor: https://mermaid-js.github.io/mermaid-live-editor/

graph TD
  Idea[Idea/problem<br>Issue]
  Feature[Feature Issue<br>or Epic]
  Release[Release Post]

  classDef idea fill:#ffffff,stroke:#19957C;
  class Idea idea

  classDef feature fill:#ffffff,stroke:#0C7CBA;
  class Feature feature

  classDef release fill:#ffffff,stroke:#E58900;
  class Release release
```

Box shapes represents whose responsibility it is to get it done:

```mermaid
graph TD
  P[Product Management]
  U(UX)
  E([Engineering])

  classDef plain fill:#ffffff,stroke:#000000;
  class P,U,E plain
```

And issue labels look like this:

```mermaid
graph TD
  Label>"~issue label"]

  classDef label fill:#ffffff,stroke:#606060;
  class Label label
```


### Validation Phase 1: Validation Backlog

```mermaid
graph LR
  A[Create idea/problem<br>issue needing validation] --> B{Needs<br>problem<br>validation}
  B -- Yes --> C[Proceed to:<br>Validation Phase 2]
  B -- No --> D[Proceed to:<br>Validation Phase 3]
  L>"~workflow::validation backlog"] --- A

  classDef idea fill:#ffffff,stroke:#19957C;
  class A idea

  classDef plain fill:#ffffff,stroke:#000000;
  class B,C,D plain

  classDef label fill:#ffffff,stroke:#606060;
  class L label
```


### Validation Phase 2: Problem Validation

```mermaid
graph TB
  2A[Create<br>Opportunity<br>Canvas] --> 2B
  2B[Attach Canvas to<br>Issue's Design tab] --> 2D
  2L>"~workflow::problem validation"] --- 2B
  2C[Rework Canvas] --> 2B
  2D("Research & <br>Interviews (with PM)") --> 2E
  2E["Finalize Canvas;<br>submit for review"] --> 2F
  2F{Approved?}
  2F -- No --> 2C
  2F -- Yes --> 2G[Proceed to:<br>Validation Phase 3]

  classDef idea fill:#ffffff,stroke:#19957C;
  class 2B idea

  classDef plain fill:#ffffff,stroke:#000000;
  class 2A,2C,2D,2E,2F,2G plain

  classDef label fill:#ffffff,stroke:#606060;
  class 2L label
```


### Validation Phase 3: Solution Validation

```mermaid
graph TB
  3A[Engage UX again] --> 3B
  3L>"~workflow::design"] --- 3A
  3B(Story mapping,<br>define JTBD,<br>solution designs) --> 3D
  3C([Early Engineering<br>input on design])
  3B --> 3C --> 3B
  3D(Design complete) -- Time to Build --> 3E[Proceed to:<br>Build Phase 1]
  3La>"~workflow::solution validation"] --- 3D

  classDef idea fill:#ffffff,stroke:#19957C;
  class 3A,3D idea

  classDef plain fill:#ffffff,stroke:#000000;
  class 3B,3C,3E plain

  classDef label fill:#ffffff,stroke:#606060;
  class 3L,3La label
```


### Build Phase 1: Plan

PM should coordinate with EMs on when refining is complete. A good signal is the presence of
a weight and/or weigh labels. The `~"workflow::refinement"` label indicates that planning is
complete and Engineering needs to review and provide the weight.

```mermaid
graph TD

  %% build p1
  L1>"~workflow::planning breakdown"]
  L2>"~workflow::scheduling"]
  A1[Final<br>Design/Solution<br>review]
  A2(Provide input<br>and design<br>adjustments)
  A3([Provide input<br>on technical<br>feasibility, sizing])
  A4([Outlines MRs for<br>implementation,<br>suggests issue<br>breakdown points])
  A5[Evaluate proposed<br>issue breakdown]
  A6{All work<br>fits in 1<br>iteration?}
  A7[Create Epic]
  A8[Create Feature Issue]
  A9(["Refined Issue(s)"])
  A9b["Assign Milestone"]
  A10[Proceed to:<br>Build Phase 2]
  L1 --- A1
  L2 --- A9
  A1 --> A5
  A1 --> A2 --> A1
  A2 --> A3 --> A2
  A3 --> A4 --> A5 --> A6
  A6 -- no --> A7 --> A9 --> A9b
  A6 -- yes --> A8 --> A9 --> A9b
  A9b --> A10

  classDef idea fill:#ffffff,stroke:#19957C;
  class A1 idea

  classDef feature fill:#ffffff,stroke:#0C7CBA;
  class A7,A8,A9,A9b feature

  classDef plain fill:#ffffff,stroke:#000000;
  class A2,A3,A4,A5,A6,A7,A8,A10 plain

  classDef label fill:#ffffff,stroke:#606060;
  class L1,L2 label
```

#### Create Epic

This is the case where Engineering determines the Idea/Problem issue is too large to be
accomplished in a single milestone. The Container Epic serves as an organizational entity as well
as single source of overall requirements from the original problem issue.

The Container Epic should have enough details to convey the overall intended value realized after
all child Feature Issues are delivered. Full design assets can be attached here for reference. This
Epic is closed when all child Issues are delivered.

All of the Feature Issues rolling-up to the Epic are their own MVCs in that they are
independently-releasable slices of value. Each issue contains its own criteria for delivery,
including any design assets relevant for just this feature.

```mermaid
graph TD

  subgraph Large
    B1[Create Epic<br>Container]
    B2[/Details from<br>original problem<br>issue/]
    B3[Individually releasable<br>Feature Issue 1]
    B4[Individually releasable<br>Feature Issue n]
    B5(Design<br>Assets)
    B6(Design<br>Assets)
    B7([Create Frontend,<br>Backend Issues])
    B8([Create Frontend,<br>Backend Issues])
    B9([Weight<br>applied])
    B10([Weight<br>applied])
    B2 --> B1
    B1 --> B3 --- B5
    B1 --> B4 --- B6
    B3 --- B7 --- B9
    B4 --- B8 --- B10
  end

  classDef large fill:#ffddd8,color:#ffddd8;
  class Large large

  classDef feature fill:#ffffff,stroke:#0C7CBA;
  class B1,B3,B4 feature

  classDef plain fill:#ffffff,stroke:#000000;
  class B2,B5,B6,B7,B8,B9,B10 plain
```


#### Create Feature Issue

This is the case where the entire scope of Idea/Problem issue can be accomplished in a single
milestone.

```mermaid
graph TD

  subgraph Small
    B1[Create Feature Issue<br>Link to original<br>problem issue]
    B3[Individually releasable<br>Feature Issue]
    B5(Design<br>Assets)
    B7([Create Frontend,<br>Backend Issues])
    B9([Weight<br>applied])
    B1 --> B3 --- B5
    B3 --- B7 --- B9
  end

  classDef small fill:#ccffce,color:#ccffce;
  class Small small

  classDef feature fill:#ffffff,stroke:#0C7CBA;
  class B1,B3 feature

  classDef plain fill:#ffffff,stroke:#000000;
  class B5,B7,B9 plain
```


### Build Phase 2: Develop & Test

```mermaid
graph TD
  L1>"~workflow::ready for development"]
  L2>"~workflow::in dev"]
  L3>"~workflow::ready for review"]
  L4>"~workflow::in review"]
  L5>"~workflow::verification"]
  P1([Commit to work in<br>Milestone, assign<br>Deliverable, Strech<br>labels])
  P2[Review Milestone<br>commitments,<br>rebalance as<br>necessary]
  P3(Demo Feedback)
  P4[Demo Feedback]
  P5[Proceed to:<br>Build Phase 3]
  R1["Create Release<br>Post MR (one per<br>feature issue)"]
  F1([Start work])
  F2([Complete work,<br>Demo])
  F3([Review work])
  F4([Verify work])
  L1 --- P1
  L2 --- F1
  L3 --- F2
  L4 --- F3
  L5 --- F4
  P1 --> R1
  P1 --> P2 --> F1 --> F2 --> F3 --> F4
  F2 --> P3 --> P4
  P4 --> P3 --> F2
  F4 --> P5

  classDef feature fill:#ffffff,stroke:#0C7CBA;
  class F1,F2,F3,F4 feature

  classDef plain fill:#ffffff,stroke:#000000;
  class P1,P2,P3,P4,P5 plain

  classDef label fill:#ffffff,stroke:#606060;
  class L1,L2,L3,L4,L5 label

  classDef release fill:#ffffff,stroke:#E58900;
  class R1 release
```


### Build Phase 3: Launch

```mermaid
graph TB
  4A["(Re)validate<br>feature works<br>for all users"]
  4B([Deploy to<br>Production]) --> 4C([Merge Release<br>Post MR])
  4L>"~workflow:production"] --- 4B
  4A --> 4B --> 4A

  classDef plain fill:#ffffff,stroke:#000000;
  class 4A plain

  classDef label fill:#ffffff,stroke:#606060;
  class 4L label

  classDef feature fill:#ffffff,stroke:#0C7CBA;
  class 4B feature

  classDef release fill:#ffffff,stroke:#E58900;
  class 4C release
```
