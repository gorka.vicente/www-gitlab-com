---
layout: handbook-page-toc
title: "The GitLab Legal Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
 
# Welcome to GitLab Legal!

We are glad you are here! Meet our [team](https://about.gitlab.com/company/team/?department=legal)

## Contacting the Legal Team with General Legal Questions

### 1. Quick Questions

You can reach out to the Legal Team on the *[`#legal`](https://gitlab.slack.com/archives/legal)* Slack chat channel.  The legal Slack chat channel is reserved for everyday legal questions that can be answered in informal communication. It is not for requests that require legal advice, deliverables, discussion of confidential information.

**Please do not share confidential information on Slack that is not meant for the entire company to see, and do not use it to seek legal advice.**

### 2. Requests with Deliverables
If you are making a request that requires some sort of deliverable, please use the list below to determine how you should reach out. If you are unsure where your non-Slack request fits, refer to [no. 6 below](https://about.gitlab.com/handbook/legal/#6-other-legal-requests). 

## Requesting Legal Services
 
How do I request the services I need?

### 1. Internal Ethics and Compliance Reporting (Anonymous)

We take employee concerns very seriously and encourage all GitLab team members to report any ethics and/or compliance violations by using Lighthouse. Futher details are found on the [People Ops Handbook page](https://about.gitlab.com/handbook/people-group/code-of-conduct/#how-to-contact-gitlabs-24-hour-hotline).

### 2. Privileged / Confidential Communications
If you have a request that involves confidential and/or sensitive information, including related to other GitLab team members, please e-mail legal@gitlab.com.  

For more information on Attorney-Client Privilege, see the [General Topics and FAQs](/handbook/legal/#general-topics-and-faqs) below. 

### 3. Customer Related Contracts and Questions

* All questions / requests for the legal team regarding Customer related matters are to follow the process stated [here](https://about.gitlab.com/handbook/business-ops/order-processing/#process-for-agreement-terms-negotiations-when-applicable-and-contacting-legal) 
* The legal team uses Contract Requests to track and manage all incoming requests. 
 
A presentation overview of the process to engage GitLab Legal can be found [HERE](https://docs.google.com/presentation/d/1lesWNvPAFd1B3RuCgKsqQlE85ZEwLuE01QpVAKPhQKw/edit#slide=id.g5d6196cc9d_2_0)

A video tutorial can be found [HERE](https://www.youtube.com/watch?v=CIWdsqRX7E0&amp=&feature=youtu.be) 

### 4. Vendor, Partner, and other "To Pay" Contracts

If you are looking for a new vendor, need an NDA for a vendor or partner, or need review of a vendor or partner contract, these services are handled by the Procurement Team. For purposes of this process, anyone that will receive payment from GitLab is considered a vendor. 

Legal will be brought in by Procurement for escalations only. Please see the [Procurement Page](/handbook/finance/procurement/) for more information on the Vendor Management Process. Once a Vendor NDA and/or Contract has been completed, it should be uploaded by the requestor into our contract management database tool [ContractWorks](/handbook/legal/vendor-contract-filing-process/). If you need a license, you will need to submit an Access Request.

### 5. Requests for Insurance Certificate

If you need an insurance certificate (other than for worker's compensation) you can send an email request directly to our insurance broker at *[ABD](mailto:abdcertdept@theabdteam.com).*    You will need to include contact information for the customer seeking to be added to the certificate and any other specific requirements relating to the coverage. If you require an insurance certificate for worker's compensation email: payroll@gitlab.com with the same information.

For a [summary of GitLab's insurance coverage](https://drive.google.com/file/d/17pvhu9cKtoQatO7xOlKbwSoOXDEJRpiQ/view?usp=sharing) please refer to this link.


### 6. Other Legal Requests 

For requests that are not Customer related, but require a deliverable such as assistance with questionnaires, engineering licenese agreement, open source question, internal operation matters, or compliance questions, please use the [Legal and Compliance Issue Tracker](https://gitlab.com/gitlab-com/legal-and-compliance/issues) using the issue template. 

**All issues should be marked as confidential. GitLab team members will be able to access these issues directly.**

Please be sure to include sufficient detail regarding your request, including time-sensitive deadlines, relevant documents, and background information necessary to respond.


 
# Contract Templates
 
- [Mutual Non-Disclosure Agreement](https://drive.google.com/a/gitlab.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)

# General Topics and FAQs

## 1. The Attorney-Client Relationship in the United States

This discussion is limited to U.S. practices because currently our team members only communicate with U.S. practicing attorneys.  As we continue to grow globally we will update this and expand how privilege applies in other jurisdictions.

### What is the Attorney-Client Privilege?

Attorney-Client Privilege is a law that has been adopted in each of the states of the U.S in some form.  Generally, the law protects communications between clients and their attorneys for the purpose of seeking legal guidance and advice.  The information is not protected if it is available from another source.  Therefore, information cannot be placed under the protections of Attorney-Client privilege simply by telling your attorney or copying your attorney on a communication.  In addition, the underlying facts are also not protected, only the opinions and analysis of the facts, and discussions thereof, with the attorney.  The privilege belongs to the client, and therefore, can only be waived by the client.

### What is Work Product?

Work Product is a U.S. doctrine in which an attorney’s notes, observations, thoughts, and research prepared by, or at the direction of an attorney, in anticipation of litigation, are protected from being discoverable during the litigation process.

### What is the purpose of these Privileges?

The purpose of the Attorney-Client and Work Product privileges is to allow clients to speak freely with their attorneys and encourage full disclosure so they can receive accurate and competent legal advice without the fear of having their attorney compelled to testify against them and disclose the information shared by the client.

### Who do these Privileges Apply to at GitLab?

There is not one uniform answer that covers all jurisdictions in the U.S. 

A minority number of states apply the **Corporate Group Test**. This test is quite restrictive and only allows for the protection of corporate communications to the corporation's controlling executives and managers.

A more commonly used test is the **Subject Matter Test**.  Instead of looking at the roles of the employees involved, this test looks at the subject matter of the employees’ communications. The test will look to see if the employee was instructed to discuss the subject matter with the attorney should be protected and if the subject matter of that communication relates to the performance by the employee of the duties of his or her employment.

A slightly modified version of the **Subject Matter Test** called the **Upjohn Test** is also widely used. Under the **Upjohn Test** the privilege is applied only if the following criteria are satisfied:

* The employee must have made the communication to counsel for the purpose of seeking legal advice regarding the corporation.
* The substance of the communication must involve matters that fall within the scope of the employee's job duties.
* The employee must be aware that the statements are being provided for the purpose of obtaining legal advice for the corporation.
* The communications also must be confidential when made and must be kept confidential by the company.

The Supreme Court case which established the  **Upjohn Test** is also important because it resulted in the **_Upjohn Warning_** which is a procedure in which a company’s attorney explains that he or she does not represent the employee individually, but instead represents the interests of the company.  This is important to note because a company can waive its privilege at any time, meaning the company could choose to disclose information the attorney received from a covered employee in confidence for use as evidence in a legal proceeding in order to protect the company from liability.
 
 The **Subject Matter Test** and **Upjohn Test** are the most commonly used tests. More information about the tests can be found [HERE](https://drive.google.com/open?id=1Rzm_oFo-nGx8RWZgSaZBOCPBL1kCrce3)
 
### How do you Protect the Privileges that Apply to You When Seeking Legal Advice?
 
 * Direct the communication to a practicing licensed attorney.  Privilege does not apply to other non-attorney members of the legal team.
 * It is best practice to have privileged conversations with the attorney via Zoom.
 * If other individuals will need to participate in the discussion, consult with the attorney, and only include the minimum necessary individuals in the conversation, in other words, keep the circle of trust small.
 * If it is necessary to communicate by email, in the “Subject” line, and at the top of the body of the communication, include the phrase **“AC PRIV”**.
 * Do not overuse the claim of privilege. Limit its use to when actually seeking legal guidance and advice and not on any and every correspondence with the attorney.
 * You must keep the information discussed confidential, and not share it with anyone outside the circle of trust without first consulting with the attorney.
 * Do not forward protected emails to anyone outside the circle of trust.
 * Do not copy anyone outside the circle of trust on emails with the attorney.
 * As with anything else, if you ever have any questions or concerns, do not hesitate to reach out to the Legal team.

## 2. Litigation Holds

### What is a Litigation Hold?

A litigation hold is the process a company uses to preserve all forms of relevant evidence, whether it be emails, instant messages, physical documents, handwritten or typed notes, voicemails, raw data, backup tapes, and any other type of information that could be relevant to pending or imminent litigation or when litigation is reasonably anticipated.  Litigation holds are imperative in preventing spoliation (destruction, deletion, or alteration) of evidence which can have a severely negative impact on the company's case, including leading to sanctions.

Once the company becomes aware of potential litigation, the company's attorney will provide notice to the impacted employees, instructing them not to delete or destroy any information relating to the subject matter of the litigation.  The litigation hold applies to paper and electronic documents. During a litigation hold, all retention policies must be overridden.

# Important Pages Related to Legal
 
* [Compliance](/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [General Guidelines](/handbook/general-guidelines/) - general company guidelines
* [Terms](/terms/) - legal terms governing the use of GitLab's website, products, and services
* [Privacy Policy](/privacy/) - GitLab's policy for how it handles personal information 
* [Trademark](/handbook/marketing/corporate-marketing/#gitlab-trademark--logo-guidelines) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents
* [Requirements for Closing Deals](/handbook/business-ops/order-processing/#step-7--submitting-an-opportunity-for-approval) - requirements for closing sales deals
* [Acceptable Licenses](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/licensing.md) - list of acceptable licenses covering third party components used in development

## Legal Processes
 
- [Generating Contracts with Conga Contracts](/handbook/legal/customer-negotiations/index.html)
- [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)

## Performance Indicators

### Administer, maintain, and manage the legal issue tracker (daily, ongoing) <= 24 hours
Triage and assign issues in the legal issue tracker to the appropriate legal team member within 24 hours of receipt, during regular business hours

### All suspicious transactions are cleared, actioned, or escalated  <= 1 Business Day
All suspicious transactions are cleared, actioned or escalated within 1 business day. This is tracked in Visual Compliance. 

### Annually review policies to ensure compliance with applicable laws,  update accordingly and communicate with business = 100% 
Over a rolling 12 months all policies to be reviewed and updated to be within compliance as documented on the Compliance Strategy Overview for the particular time period.  All updates are communicated with business. 

### Audit Open Source License compliance with policy = 100%
On a monthly basis audit all new open source licenses to ensure compliance with the policy after each release. 
Ensure proper license types are being used

### Ensure all federal government submissions, representations, and certifications are audited and accurate = 100%  
Verify all federal government submissions, representations and certifications are reviewed and accurate.

### Ensure all fully executed vendor contracts are in ContractWorks = 100%
Administer, maintain, and manage ContractWorks by ensuring all fully executed vendor contracts are uploaded with terms, and that all fields are complete. This will be measured on a monthly basis and the target is 100%.

### Ensure all fully executed sales contracts are in the Salesforce = 100%
Administer, maintain, and manage Salesforce by ensuring all fully executed sales contracts are uploaded with terms, and that all fields are complete. This will be measured on a monthly basis and the target is 100%.

### Ensure protections over corporate trademarks = 100%
File annual registrations and respond to challenges to intellectual property rights throughout the year based on registration dates of trademarks. This is tracked in Marcaria

### Negotiation Cycle Average Days per Quarter <= 90 days
Average number of days on a quarterly basis in “Negotiating” of 90 days or less. This is contingent upon the updated SFDC Legal operations model. There will be a report that shows when a contract negotiation begins, and when it is closed.  

### Number of Opportunities closed by Contract Manager(s) per Quarter >= 66
The average number of Opportunities (with contracting needs) closed per quarter to be equal or greater than 66, with annual total of 264 per Contract Manager. This is contingent upon the number of contracts brought forward by the sales team. 

### Percentage of contract negotiations per quarter <=15% 
This is calculated by taking the number of opportunities closed per quarter by Contracts Managers divided by the total number of opportunities closed per quarter. In the future this will be tracked in Salesforce. 

### Response times to initial requests for review <= 24 business hours
Monthly average response time within 24 business hours in the future this will be tracked in Salesforce for all Contract Managers. 

### ‘Turn-Around’ times on received red-lines <= 72 business hours
Monthly average red-lines / legal answer(s) within 72 business hours in the future this will be tracked in Salesforce for all Contract Managers. This is contingent on type of Agreement (MSA vs. NDA).
 
### Vendors and applicable commercial partners agree to Partner Code of Ethics = 100%
Strive for 100% compliance on vendors and applicable commercial partners agreeing to Partner Code of Ethics. This will be audited by Internal Audit.





