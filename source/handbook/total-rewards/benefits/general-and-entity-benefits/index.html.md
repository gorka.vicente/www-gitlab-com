---
layout: handbook-page-toc
title: General & Entity Specific Benefits
description: A list of the General & Entity Specific Benefits that GitLab offers.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

For the avoidance of doubt, the benefits listed below in the General Benefits section are available to contractors and employees, unless otherwise stated. Other benefits are listed by countries that GitLab has established an entity or co-employer and therefore are applicable to employees in those countries only via our entity specific . GitLab has also made provisions for Parental Leave which may apply to employees and contractors but this may vary depending on local country laws. If you are unsure please reach out to the Total Rewards team.

## Entity Specific Benefits
- [GitLab BV (Netherlands)](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-netherlands)
- [GitLab BV (Belgium)](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-belgium)
- [GitLab Lyra (India)](/handbook/total-rewards/benefits/general-and-entity-benefits/lyra-benefits-india)
- [GitLab Inc (US)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us)
- [GitLab Inc (China)](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-china)
- [GitLab LTD (UK)](/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk)
- [GitLab GmbH (Germany)](/handbook/total-rewards/benefits/general-and-entity-benefits/gmbh-benefits-germany)
- [GitLab PTY (Australia)](/handbook/total-rewards/benefits/general-and-entity-benefits/pty-benefits-australia)
- [GitLab Canada Corp](/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits)
- [Safeguard (Ireland, Hungary and Spain)](/handbook/total-rewards/benefits/general-and-entity-benefits/safeguard/)
- [CXC (New Zealand)](/handbook/total-rewards/benefits/general-and-entity-benefits/cxc/)

### Benefits Available to Contractors

Contractors of GitLab BV are eligible for the [general benefits](/handbook/total-rewards/benefits/#general-benefits), but are not eligible for entity specific benefits as they have an additional 17% added to the [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator). A contractor may bear the costs of their own health insurance, social security taxes and so forth, leading to a 17% higher compensation for the contractor within the calculator.

NOTE: Our contractor agreements and employment contracts are all on the [Contracts](/handbook/contracts/) page.

## General Benefits

### Spending Company Money

GitLab will [pay for the items you need to get your job done](/handbook/spending-company-money).

### Stock Options

[Stock options](/handbook/stock-options/) are offered to most GitLab team members. We strongly believe in employee ownership in our Company. We are in business to create value for our shareholders and we want our employees to benefit from that shared success.

### Life Insurance

In the unfortunate event that a GitLab team member passes away, GitLab will provide a [$20,000](/handbook/total-rewards/compensation/#exchange-rates) lump sum to anyone of their choosing. This can be a spouse, partner, family member, friend, or charity.
* For US based employees of GitLab Inc., this benefit is replaced by the [Basic Life Insurance](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#basic-life-insurance-and-add).
* For all other GitLab team members, the following conditions apply:
  * The team member must be either an employee or direct contractor.
  * The team member must have indicated in writing to whom the money should be transferred. To do this you must complete the [Expression of wishes](https://docs.google.com/document/d/1bBX6Mn5JhYuQpCXgM4mkx1BbTit59l0hD2WQiY7Or9E/edit?usp=sharing) form. To do this, first copy the template to your Google Drive (File -> Make a copy), enter your information, sign electronically. To sign the document, use a free document signing program like [smallpdf](https://smallpdf.com/sign-pdf) or [HelloSign](https://app.hellosign.com/); or you can print it, sign and digitize. Sign, save as a pdf and upload to your Employee Uploads folder in BambooHR.
  * For part-time GitLab team members, the lump sum is calculated pro-rata, so for example for a team member that works for GitLab 50% of the time, the lump sum would be [$10,000](/handbook/total-rewards/compensation/#exchange-rates).

### Paid Time Off

GitLab has a "no ask, must tell" time off policy [time off policy](/handbook/paid-time-off) per 25 consecutive calendar days off.

### Tuition Reimbursement

Full-time team members who have been employed for three months are eligible to participate in GitLab's [tuition reimbursement program](/handbook/people-group/code-of-conduct/#tuition-reimbursement).

### GitLab Contribute
Every nine months or so GitLab team members gather at an exciting new location to [stay connected](/blog/2016/12/05/how-we-stay-connected-as-a-remote-company/), at what we like to call [GitLab Contribute](/company/culture/contribute). It is important to spend time face to face to get to know your team and, if possible, meet everyone who has also bought into the company vision. There are fun activities planned by our GitLab Contribute Experts, work time, and presentations from different functional groups to make this an experience that you are unlikely to forget! Attendance is optional, but encouraged. For more information and compilations of our past events check out our [previous Contributes (formerly called GitLab Summit)](/company/culture/contribute/previous).

### Business Travel Accident Policy

[This policy](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPVl9rYW4tXzIyeUlMR0hidWIzNk1sZjJyLUhB/view?usp=sharing) provides coverage for team members who travel domestic and internationally for business purposes. This policy will provide Emergency Medical and Life Insurance coverage should an emergency happen while you are traveling. In accompaniment, there is coverage for security evacuations, as well a travel assistance line which helps with pre-trip planning and finding contracted facilities worldwide.
   * Coverage:
      - Accidental Death [enhanced coverage]: 5 times Annual Salary up to USD 500,000.
      - Out of Country Emergency Medical: Coverage up to $250,000 per occurrence. If there is an injury or sickness while outside of his or her own country that requires treatment by a physician.
      - Security Evacuation with Natural Disaster: If an occurrence takes place outside of his or her home country and Security Evacuation is required, you will be transported to the nearest place of safety.
      - Personal Deviation: Coverage above is extended if personal travel is added on to a business trip. Coverage will be provided for 25% of length of the business trip.
      - Trip Duration: Coverage provided for trips less than 180 days.
      - Baggage & Personal Effects Benefit: $500 lost bag coverage up to 5 bags.
   * For any assistance with claims, please reference the [claims guide (internal only)](https://drive.google.com/file/d/1vmLjhebsf81N8oSxqlCihYg5q1WT8Efw/view?usp=sharing).
   * This policy will not work in conjunction with another personal accident policy as the Business Travel Accident Policy will be viewed as primary and will pay first.
   * For more detailed information on this benefit, please reference the [policy document](https://drive.google.com/file/d/1ktx_mhlEYyQoLrQJ7DhIcibQhrlnB-lb/view?usp=sharing).
   * If you need a confirmation of coverage letter, please reference the [visa letter generation document (internal only)](https://drive.google.com/file/d/1oesZnp-fVWWCakVejB7nTV39lntMFnSd/view?usp=sharing).
   * For any additional questions, please contact the Total Rewards Analyst.

### Immigration

GitLab offers benefits in relation to [obtaining visas and work permits](/handbook/people-group/visas/) for eligible team members.

### Employee Assistance Program

GitLab offers an Employee Assistance Program to all team members via [Modern Health](/handbook/total-rewards/benefits/modern-health).

### Incentives

The following incentives are available for GitLab team members:
   - [Sales Target Dinner Evangelism Reward](/handbook/incentives/#sales-target-dinner)
   - [Discretionary Bonuses](/handbook/incentives/#discretionary-bonuses)
   - [Referral Bonuses](/handbook/incentives/#referral-bonuses)
   - [Visiting Grant](/handbook/incentives/#visiting-grant)

### All-Remote

GitLab is an [all-remote](/blog/2018/10/18/the-case-for-all-remote-companies/) company; you are welcome to [read our stories](/company/culture/all-remote/stories/) about how working remotely has changed our lives for the better.

You can find more details on the [All Remote](/company/culture/all-remote/) page of our handbook.

_If you are already a GitLab employee and would like to share your story, simply add a `remote_story:` element to your entry in `team.yml` and it will appear
on that page._

### Part-time contracts

As part of our Diversity & Inclusion value, we support [Family and friends first](/handbook/values/#family-and-friends-first-work-second) approach. This is one of the many reasons we offer part-time contracts in some teams.

We are growing fast and unfortunately, not all teams are able to hire part-time employees yet. There are certain positions where we can only hire full-time employees.

Candidates can ask for part-time contract during the interview process. Even when a team they are interviewing for can't accept part-time employees, there might be other teams looking for the same expertise that might do so. If you are a current team member and would like to switch to a part-time contract, talk to your manager first.

### Parental Leave

Anyone (regardless of gender) at GitLab who becomes a parent through childbirth or adoption is able to take parental leave. GitLab team-members will be encouraged to decide for themselves the appropriate amount of time to take and how to take it. We will offer everyone who has been at GitLab for six months and completed a probationary period (if applicable) up to 16 weeks of 100% paid time off during the first year of parenthood. We encourage parents to take the time they need.

For many reasons, a team member may require more time off for parental leave. Many GitLab members are from countries that have longer standard parental leaves, occasionally births have complications, and sometimes 16 weeks just isn't enough. Any GitLab team member can request additional unpaid parental leave, up to 4 weeks. We are happy to address anyone with additional leave requests on a one-on-one basis. All of the parental leave should be taken in the first year.

If you have been at GitLab for a six months and completed your probationary period your parental leave is fully paid. If you've been at GitLab for less than a six months it depends on your jurisdiction. If applicable, commissions are paid while on parental leave based on the prior six months of performance with a cap at 100% of plan. For example, if in the six months prior to starting parental leave you attained 85% of plan, you will be compensated at the same rate while on leave. On the day you return from leave and going forward, your commissions will be based on current performance only.

You are entitled to and need to comply with your local regulations. They override our policy.

To [alleviate the stress](/handbook/paid-time-off/#returning-from-work-after-parental-leave) associated with returning to work after parental leave, GitLab supports team members coming back at [50% capacity](/handbook/paid-time-off/#returning-to-work-at-50-capacity). Parents at GitLab who are reentering work following parental leave are encouraged to reach out to team members who self-designate as a [Parental Leave Reentry Buddy](/handbook/total-rewards/benefits/parental-leave-buddy/).

Managers of soon to be parents should check out this [Parental Leave Manager Tool Kit](/handbook/total-rewards/benefits/parental-leave-buddy/#manager-tool-kit) for best practices in supporting your team members as they prepare for and return from Parental Leave.

**How to Apply for Parental Leave**

1.  To initiate your parental leave, please send an email to Total Rewards Analysts (`total-rewards@gitlab.com`) and your manager at least thirty days before your leave will begin, and also take a look at the process for application based on your employment company Country Specific Leave Requirements, which are subject to change as legal requirements change:

    * [GitLab Inc. United States Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#gitlab-inc-united-states-leave-policy)
    * [GitLab Inc. China Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-china/#gitlab-inc-china-leave-policy)
    * [GitLab B.V. Netherlands Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-netherlands/#gitlab-bv-netherlands-leave-policy)
    * [GitLab B.V. Belgium Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/bv-benefits-belgium/#gitlab-bv-belgium-leave-policy)
    * [GitLab B.V. India Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/lyra-benefits-india/#gitlab-bv-india-leave-policy)
    * [GitLab LTD United Kingdom Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/ltd-benefits-uk/#gitlab-ltd-united-kingdom-leave-policy)
    * [GitLab GmbH Germany Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/gmbh-benefits-germany/#gitlab-gmbh-germany-leave-policy)
    * [GitLab PTY Australia Leave Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/pty-benefits-australia/#gitlab-pty-australia-leave-policy)

    If you need any additional leave, please refer to our [Paid Time Off Policy](/handbook/paid-time-off).

    If you're interested in learning about how other GitLab team members approach parenthood, take a look at [the parenting resources wiki page](https://gitlab.com/gitlab-com/gitlab-team-member-resources/wikis/parenting) and [#intheparenthood](https://gitlab.slack.com/messages/CHADS8G12/) on Slack.

2. Process for Total Rewards Analysts:
  * Log and monitor upcoming parental leave in the "Parental Leave" Google sheet on the drive.
  * Once the leave is official:
    * File all documentation in BambooHR.
    * Update the Employment Status of the team member to "Parental Leave" with the date the team member started leave.
  * Add another entry with the date the team member will return from leave with the status "End of Parental Leave" and in the comment section, add "Tentative". This will generate a notification to the Compensation & Benefits team 1 day after the team member returns from leave.
  * Notify payroll.
  * Once BambooHR notification is received:
    * Change "End of Parental Leave" status to "Active" and in the comment section, add "Return from leave".
    * Update payroll of the team's return date.
