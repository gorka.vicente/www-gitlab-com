---
layout: markdown_page
title: "Content Syndication"
---

## Overview
This page focuses on content syndicatio through external vendors as a tactic used within marketing campaigns, including account centric campaigns. When employing this tactic, we have promoted our content through a third-party vendor, but do not direct the end users back to our website. In these cases, we often have given them the resource to make available for download to their audience, and recieve the leads to be uploaded.

### Process in GitLab to organize epic & issues

The FMM (or requester) is responsible for following the steps below to create the epic and related issues in GitLab.

1. FMM creates the main tactic issue
1. FMM creates the epic to house all related issues (code below)
1. FMM creates the relevant issues required (shortcut links in epic code below)
1. FMM associates all the relevant issues to the newly created epic, as well as the original issue
1. FMM sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the FMM/requester is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

**Name: Content Syndication - [Vendor] - [3-letter Month] [Date], [Year]**

```
<--- Name this epic using the following format, then delete this line: Content Syndication - [Vendor] - [3-letter Month] [Date], [Year] --->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **FMM/Requester:** 
* **MPM:**  
* **FMC:** 
* **Type:** Content Syndication
* **Account Centric?** Yes/No
* **Budget:** 
* **Launch Date:**  [MM-DD-YYYY] (this is the date the content syndication begins through the vendor)
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] campaign utm `` - FMM to fill in based on budget doc

## User Experience
[FMM to provide a description of the user journey - from communication plan, to what the user experiences upon reciept, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - FMM creates, assign to MPM
* [ ] [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMM creates, assign to FMM and MOps
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - FMM creates, assign to FMM, MPM and FMC
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - FMM creates, assign to FMM and MPM

/label ~"Marketing Programs" ~"Field Marketing" ~mktg-status:wip ~"Content Syndication"
```

=====NEED TO MOVE THIS TO MPM HANDBOOK=====

# Steps to Setup Content Syndication in Marketo/SFDC

### Step 1: (Clone this program](https://page.gitlab.com/#PG4452A1)
* Use format `YYYY_ContSynd_NameofAsset`
* If the content syndication is part of a package with an external vendor, promoting several assets or webcasts, keep all of the Marketo programs together in a folder for easy access as part of a single vendor program.

### Step 2: Sync to Salesforce
* At the program main screen in Marketo, where it says `Salesforce Sync` with "not set", click on "not set"
    * Click "Create New" and "Save"
* Now go to the [content campaign view in Salesforce](https://gitlab.my.salesforce.com/701?fcf=00B61000004NY3B&rolodexIndex=-1&page=1) and click into your campaign, by the same name
    * Change the `Camapign Sub-Type` to `Content Syndication`
    * Change the `Enable Bizible Touchpoints` to `Include only "Responded" Campaign Members`
    * Update the event epic
    * Update the description
    * Update `Start Date` to the date of launch
    * Update `End Date` to 90 days from date of launch (if this is an ongoing campaign, update appropriately)
    * Upaded `Budgeted Cost` if you have the data available
    * Click "Save" (you must do this before you update campaign owner or it will erase your edits)
    * Now, change `Campaign Owner` to your name

### Step 3: Update Marketo tokens
* Change the `Content Title` to be the title as it appears in the Gated Content program
* Change the `Content Type` to be the type of content
   * The only available options are `Whitepaper`, `eBook`, `Report`, `Video`, or `General`
   * If you add a Content Type value other than the above, the record will hit an error when syncing to Salesforce because these are the only currently available picklist items for `Initial Source`

### Step 4: Activate Marketo smart campaign
* In the `01 downloaded` smart campaign, the "Smart List" should have a flow listening for "Program Value Change [to] Gated Content > Downloaded"
   * The correct program should automatically apply when cloned, so *you don't need to do anything here.*
* In the `01 downloaded` smart campaign, the "Flow" will trigger an interesting moment to be applied, the score will increase +30* and the `Person Source` (note: this maps to `Initial Source` in Salesforce) will update IF a `Person Source` does not already exist (i.e. it is blank)
    * Quick note on the score increase: we will remove the scoring flow from the smart campaign in this template when scoring is handled by program membership triggers at the systematic level in the future (dependent on a lead scoring revamp and best practice implementation)
* Click to the "Schedule" tab and click `Activate`.
    * Wehn the leads are loaded to the campaign by Marketing Ops, the leads will immediately have an interesting moment, +30 score, and initial source update as needed.