---
layout: markdown_page
title: "External Virtual Events"
---

# Types of external virtual events

External virtual events are, by definition, not owned and hosted by GitLab. They are hosted by an external third party (i.e. DevOps.com). The two types of external virtual events are below, and involve epic and issue creation, designation of DRIs, and workback schedule definition between requester (commonly FMM or Corporate events) and Marketing Programs.

* [Sponsored Webcasts](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#sponsored-webcast): A sponsored webcast is hosted on an external partner/vendor platform (e.g: DevOps.com). The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform, and delivering a lead list after the event.
* [Virtual Conferences](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#virtual-conference): In a virtual conference, GitLab will pay a sponsorship fee to receive a virtual booth and sometimes a speaking session slot or panel presence. **Presence of a virtual booth is a requirement for the external virtual event to be considered a Virtual Conference.**

## 📌 Sponsored Webcast

*A sponsored webcast is hosted on an external partner/vendor platform (e.g: DevOps.com). The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform, and delivering a lead list after the event. The owner (FMM in most cases) is responsible for the epic and related issue creation. Mktg-OPs will be responsible for uploading the list to our database and MPMs will be responsible for sending post-event follow-up emails (when relevant).*

### Process in GitLab to organize epic & issues

The FMM or requester is responsible for following the steps below to create the epic and related issues in GitLab.

1. FMM creates the main tactic issue
1. FMM creates the epic to house all related issues (code below)
1. FMM creates the relevant issues required (shortcut links in epic code below)
1. FMM associates all the relevant issues to the newly created epic, as well as the original issue
1. FMM sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the FM/requester is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

**Name: Sponsored Webcast - [Vendor] - [3-letter Month] [Date], [Year]**

```
<--- Name this epic using the following format, then delete this line: Sponsored Webcast - [Vendor] - [3-letter Month] [Date], [Year] --->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **FMM/Requester:** 
* **MPM:**  
* **FMC:** 
* **Type:** Sponsored Webcast
* **Account Centric?** Yes/No
* **Budget:** 
* **Launch Date:**  [MM-DD-YYYY] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] campaign utm `` - FMM to fill in based on budget doc

## User Experience
[FMM to provide a description of the user journey - from communication plan, to what the user experiences upon reciept, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - FMM creates, assign to MPM
* [ ] [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMM creates, assign to FMM and MOps
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - FMM creates, assign to FMM, MPM and FMC
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - FMM creates, assign to FMM and MPM
* [ ] [Gated content request issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Request-MPM) (*optional - only if we have rights to recording and content is worth gating*) - FMM creates, assign to FMM, MPM and FMC (this will be gated as a Video, not as an on-demand webcast)

/label ~"Marketing Programs" ~"mktg-status::wip" ~"Webcast - Sponsored" ~"Field Marketing"
```

☝️ *Note on campaign utm format: we avoid using special characters due to issues in the past passing UTMs from Bizible to SFDC, the basis for attribution reporting.*

## 📌 Virtual Conference

*In a virtual conference, GitLab will pay a sponsorship fee to receive a virtual booth and sometimes a speaking session slot or panel presence. MPMs will support in creating the epic and issue, and will primarily be responsible for sending the post-event follow-up email and adding to the nurture.*

**Presence of a virtual booth is a requirement for the virtual event to be considered a Virtual Conference.** [Link to Marketo program template that will be cloned.](https://app-ab13.marketo.com/#ME4739A1)

### Process in GitLab to organize epic & issues

The MPM is responsible for following the steps below to create the epic and related issues in GitLab.

1. FMM creates the main tactic issue
1. FMM creates the epic to house all related issues (code below)
1. FMM creates the relevant issues required (shortcut links in epic code below)
1. FMM associates all the relevant issues to the newly created epic, as well as the original issue
1. FMM sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the FMM (or other requester) is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

**Name: Virtual Conference - [Vendor] - [3-letter Month] [Date], [Year]**

```
<--- Name this epic using the following format, then delete this line: Virtual Conference - [Vendor] - [3-letter Month] [Date], [Year] --->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **FMM/Requester:** 
* **MPM:**  
* **FMC:** 
* **Type:** Virtual Conference
* **Account Centric?** Yes/No
* **Budget:** 
* **Launch Date:**  [MM-DD-YYYY] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] campaign utm `` - FMM to fill in based on budget doc

## User Experience
[FMM to provide a description of the user journey - from communication plan, to what the user experiences upon reciept, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - FMM creates, assign to MPM
* [ ] [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMM creates, assign to FMM and MOps
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - FMM creates, assign to FMM, MPM and FMC
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - FMM creates, assign to FMM and MPM
* [ ] [Gated content request issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Gated-Content-Request-MPM) (*optional - only if we have rights to recording and content is worth gating*) - FMM creates, assign to Requester and MPM (this will be gated as a Video, and should be created for each video we will receive)

/label ~"Marketing Programs" ~"mktg-status::wip" ~"Virtual Conference" ~"Field Marketing"
```
