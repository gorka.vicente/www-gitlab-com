---
layout: handbook-page-toc
title: "Forum response workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

New posts to the GitLab forum are brought in to Zendesk and #gitlab-forum on Slack. 

The best place to engage on the forum as a Community Advocate, or anyone representing GitLab, is directly in the [forum platform](https://forum.gitlab.com). 

## Best practices

- Always be courteous and generous, especially if they are a new poster. 
- Add a welcome message when replying to new posters. It could be as simple as "Hi, and welcome to the forum! :smile:" at the start of your post.
- Use the like button as much as you can, to thank users for their input and to inspire other users to do the same.
- Use the solution checkbox button as much as you can, to indicate to others when a forum topic has an answer.

## Administration

Most administration tasks will be done from the [Discourse Admin Dashboard](https://forum.gitlab.com/admin)

### How to Grant Admin Permissions

If you want to add an admin:

1. Go to the [list of forum users](https://forum.gitlab.com/admin/users/list/active)
2. Use the search box to find the user you want to grant admin permissions to
3. Click on the user to modify their profile
4. Scroll down to the `Permissions` section
5. Click on the `Grant Admin` button
6. An e-mail confirmation will be sent to the Admin that granted the permission (i.e. you). Go to your inbox and click on the link to confirm granting Admin permission to the user
7. If all went well, the `Permissions` > `Admin?` section on the user's profile admin should read `Yes`

Note: we would like to reserve only one or two Staff spots for forum Admins within our existing 15 total Staff spots. 

### How to Grant Moderator Permissions

If you want to add an Moderator:

1. Go to the [list of forum users](https://forum.gitlab.com/admin/users/list/active)
2. Use the search box to find the user you want to grant moderator permissions to
3. Click on the user to modify their profile
4. Scroll down to the `Permissions` section
5. Click on the `Grant Moderation` button
7. Immediately the `Permissions` > `Admin?` section on the user's profile admin should read `Yes`

Note: we only have 15 Staff spots available with our current Discourse subscription. Please check with an Admin (Lindsay Olson) if you want to grant, or want to receive, moderator status. Right now, the Advocates team + four others have Moderator status. 

### Moderator Specific Permissions

Those with Moderator status will see a different grey toolbar on forum posts: 

1. Click the elipses (...)
2. Utilize the solutions checkbox, the edit tool, and the delete options as necessary

## Workflow

How to respond to the GitLab forum tickets: 

1. Review the tickets in Zendesk `GitLab Forum` view
2. See if all comments have received responses
3. Write the response if appropriate, involve an expert for assistance if you don't know the answer
4. Post the comment on the original website ([https://forum.gitlab.com](https://forum.gitlab.com), not Zendesk) using the link provided in the ticket
5. Solve the ticket with the `Replied` macro (Replied macro will use the public response field in order to track the first reply time)

Whether you work through forum posts via Zendesk, or the [forum platform](forum.gitlab.com), consider using some of the following resources to help get answers to questions: 
* Search the forum for related topics - you may find a community member who has solved something similar. You can loop them into the conversation or point the user to a thread that might be helpful
* Search [GitLab's documentation](https://docs.gitlab.com) for helpful documentation to aid in troubleshooting
* [Search Support's Zendesk instance for solved tickets}(/handbook/marketing/community-relations/community-advocacy/workflows/twitter/#using-gitlab-support-zendesk) about the same issue the forum user is having and share that knowledge in the forum thread

### Moderator and Admin Workflow

How and when to edit forum posts:
* Violation of GitLab's [Code of Conduct](https://about.gitlab.com/company/culture/contribute/coc/) (Ex. redacting swear words, if they are unable or unwilling to edit it themselves)
* Redact private information (Ex. license keys, account info, email addresses, etc.)
* Editing can be achieved via the grey pencil icon in the post toolbar (... > pencil)

How and when to mark a solution in a thread:
* When there is a clear answer to the topic and/or question
* Makrking the solution can be done  via the grey checkbox icon in the post toolbar (... > checkbox)

How and when to delete a forum post or topic:
* Please do not delete other's posts. When necessary, message the user and let them delete it themselves
* Deleting content for any reason is a breach of trust, which is something we are working hard to build in our forum community. If you feel like a post needs to be deleted, please reach out to an Admin, or Lindsay Olson, so the situation can be discussed


Follow the relevant workflow depending on the question:


```mermaid
graph TD
A((Non-support)) --> B
B(Account) --> |Sales/Renewal| E[ping #sales]
B --> |Other|F(Have they opened a support ticket?)
F --> |Yes, using GitLab.com| G[ping #support_gitlab-com]
F --> |Yes, self-managed| H[ping #support_self-managed]
F --> |No| I[Direct them to support.gitlab.com]
A --> J(GitLab vs Competitor)
J --> |Question|K[Find answer via devops tools page/blog and link]
J --> |Discussion|L[Link on #competition]
A --> M(Feature Proposal)
M --> |Already exists|N[Link to the open issue]
M --> |Doesn't exist|O[Ask them to open an issue]
```

```mermaid
graph TD
A((Support)) --> B(What level of GitLab are they on?)
B --> |Not free|C(Have they opened a support ticket?)
C -->|No| E[Link them to the support portal]
C -->|Yes| F(Have they provided the ticket number?)
F --> |No|R(Ask them for the ticket number)
R --> F
F --> |Yes|G(Self-hosted or .com?)
G --> |Self-hosted|H[ping #support_self-managed]
G --> |.com|I[ping #support_gitlab-com]
G --> |not provided|J(Ask what they're using)
J --> G
B-->|Free| D(Is there an issue open already?)
D --> |Yes|K[Link the issue]
D --> |ask the following questions|L(Self-hosted or .com?)
L --> M(What version are they on?)
M --> N(Has this happened on a previous version?)
N --> O(How long have they been experiencing it?)
O --> P(What integrations do they have, if relevant?)
P --> |Questions answered|Q[ping relevant product channel]
```

## GitLab Forum Strategy

Engagement drives engagment, so the more the GitLab team engages with our wider community, the more they are likely to engage with us, and with others. By setting the example of providing thorough answers, we can build our forum up as a place of knowledge share and collaboration. [See the Forum's 2020 strategy slide deck for more](https://docs.google.com/presentation/d/1PiNlxFImSIO8kz9TfWMZ6GLGd9fYefILpC6LS3w3lJE/edit#slide=id.p). 

## Automation

New mentions are brought into Slack and Zendesk via Zapier.
