---
layout: handbook-page-toc
title: "Finance Operations"
description: "Business Technologies Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Contact

* [Project](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems)
   * [Issues](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/issues/new?issuable_template=general_issue)
* Slack channel #bzo-finance-operations
* GitLab group `@gitlab-com/business-ops/financeops`

## Purpose

##### Applications Managed

* ADP
* Amex
* Avalara
* Carta
* ContractWorks
* Expensify
* FloQast
* Netsuite
* Stripe
* Tipalti
* Trip Actions
* Workiva
* Xactly
* Zuora
