---
layout: handbook-page-toc
title: "Being a public company"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Being a public company

* As detailed in our [company strategy](/company/strategy/), GitLab wants to go public in CY2020, specifically on Wednesday, November 18, 2020.
* We have a [strategy with clear goals for November 18, 2023](/company/strategy/#sequence) such as getting to $1B ARR.

## Public Company Readiness

To achieve fair value with limited relative volatility as a publicly traded company, we are focused on the following:
* Ability to meet the quarter close schedule outlined on the [Investor Relations](https://about.gitlab.com/handbook/finance/investor-relations/#earnings-release-calendar-workback-schedule) page
* Consistent execution against key operating metrics and initiatives
* Demonstrate a reliable trajectory of revenue growth to operating cash flow that aligns to our target [operating model](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/#long-term-profitability-targets) and [hypergrowth](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/hypergrowth-rule/#the-hypergrowth-rule) objectives
* Assessing the maturity of our model in conjunction with equity market volatility to determine an appropriate time for a public offering
* Building out a fulsome compliance program which includes: securities law trainings, communications policies, handbook adjustments, required policies such as insider / [trading window](https://about.gitlab.com/handbook/finance/investor-relations/#trading-window)
* Maintaining a high degree of [transparency](https://about.gitlab.com/handbook/values/#transparency) that we believe increases contributions and makes collaboration easier.  This is a core value and may result in higher than expected volatility.  

A successful public offering is a significant milestone, but it is not GitLab’s [mission](https://about.gitlab.com/company/strategy/#mission).  Like graduating from high school, a great day but it shouldn't be the biggest thing you achieve in life.

## Market Capitalization

Market capitalization (stock price x shares outstanding) will be the result of a combination of factors most directly associated with the following areas:

* Size of [total addressable market](https://about.gitlab.com/handbook/sales/tam/)
* Consistency of execution
* [Competitive position](https://about.gitlab.com/handbook/leadership/biggest-risks/#introduction)
* Clarity of [value proposition](https://about.gitlab.com/company/strategy/#organization)
* Perception of management [team](https://about.gitlab.com/company/team/) 

As GitLab team members, delivering on our annual plan and long-term strategy are the most productive ways to contribute to the company achieving full, fair value with limited relative volatility.  At a departmental level, we have [key performance indicators](https://about.gitlab.com/handbook/ceo/kpis/#what-are-kpis) aligned to our plan and performance to empower "everyone to contribute" to GitLab’s long term success and public market valuation.

A finer point on valuation: In the long run, our underlying business performance will be the fundamental driver of GitLab's stock price. Workplace conversations on the stock price can be a distraction; we should instead shift discussions to our KPIs and focus on growing [incremental annual contract value](https://about.gitlab.com/handbook/sales/#sts=Incremental%20Annual%20Contract%20Value%20(IACV)). The father of value investing, Benjamin Graham, explained this concept by saying, "In the short run, the market is like a voting machine, tallying up which firms are popular and unpopular. But in the long run, the market is like a weighing machine, assessing the substance of a company." 

## Long Term Targets Underpin Efficient, Durable Growth

Our financial planning process includes setting a division's spend based on revenue growth as we move towards our long term profitability target. We view these spending objectives (as a percent of revenue) as a measure of our efficient use of capital. **These objectives are not because we aim to go public.** As we capture a larger percentage of the market, growth could moderate and shift operating cash flow margin to a more significant factor in our [hypergrowth](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/hypergrowth-rule/#the-hypergrowth-rule) target.  

## Transparency

* Transparency is a [core value of GitLab](/handbook/values/#transparency).
* We want to maintain the same overall level of transparency as a public company that we had as a private company. For example, indicators of revenue such as our [win rate](/handbook/sales/#win-rate) will no longer be accessible externally, but in other areas transparency will be increased: financial profile.   
* We strive to be the most transparent public company in the world and believe our practice of [Transparency](https://about.gitlab.com/handbook/values/#transparency) will lead to GitLab setting a new standard for public companies.
* When we are required to publicly release quarterly financials, we may be required to adjust the timing of or level of detail associated with certain key metrics. 

## Disclaimer

Statements on this page are being made pursuant to, and in accordance with, Rule 135 under the Securities Act of 1933, as amended (the “Securities Act”) and shall not constitute an offer to sell, or the solicitation of an offer to buy, any securities. Any offers, solicitations or offers to buy, or any sales of securities will be made in accordance with the registration requirements of the Securities Act.

## Remaining an Independent Company

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/LIXLGyZK72c" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab co-founder and CEO [Sid Sijbrandij](https://gitlab.com/sytses) discusses the topic of remaining independent as a company with [Kristóf Éger](https://gitlab.com/keger). The [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) is transcribed, in part, below.*

> Our intention, from the moment we took external funding, was to stay independent. 
> 
> The reason we want to stay independent is we think it will better allow us to preserve our culture — we have our [six values](/handbook/values/) that are important to us — and also to be a good steward of open source. 
>
> Now, we're not totally in control of that. The majority of GitLab is owned by venture capitalists. But, we do have some sway. If, as an executive team, you're not interested in being acquired, it's harder to acquire a company. 
>
> We're always more [optimistic](/handbook/values/#focus-on-improvement) about the future than anybody outside of the company. That means that we have to keep growing. We have to keep growing [IACV](/handbook/sales/#incremental-annual-contract-value-iacv), and keep growing our revenue. 
>
> We keep investing in the future in order to not get bought. We've been very clear with our investors what our intentions were from the start. We're doing everything we can to stay independent. - *GitLab co-founder and CEO Sid Sijbrandij* 

