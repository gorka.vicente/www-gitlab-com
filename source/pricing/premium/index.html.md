---
layout: markdown_page
title: "Why Premium/Silver?"
---

## GitLab Premium/Silver

Premium / Silver is ideal for scaling organizations and for multi team usage. Premium enables enterprises scale their DevOps delivery with progressive deployment, advanced configuration, and consistent standards. Premium enables enterprises to:

## Increase Operational Efficiencies
To cater to scaling enterprises, Premium introduces capabilities that allow enterprises analyze team, project and group trends to uncover patterns and setup consistent standards to improve overall productivity. With the introduction of the Service Desk capabilities in Premium, a feedback loop from customers back to development is established, thereby allowing teams to spend more time on capabilities that bring maximum value to your customers.
> **90% of Paessler's QA is self served - the QA engineer’s tasks have been slashed from about an hour a day to 30 seconds, a 120x speed increase** <br><br> Every branch gets tested, it’s built into the pipeline. As soon as you commit your code, the whole process kicks off to get it tested. The amount of effort involved in actually getting to the newest version that you’re supposed to be testing, whether you’re a developer or a QA engineer, is minimized immensely. <br> ***Greg Campion*** <br> Senior Systems Administrator, Paessler <br> [Read more](/customers/paessler/)

## Deliver Better Products Faster
Premium enables organizations to minimize deployment risks by allowing incremental rollout of your product to end users. Premium supports enterprise grade high availability, disaster recovery and replication for geographically distributed organizations, thereby delivering improved uptime and performance.

> **Goldman Sachs improves from 1 build every two weeks to over a thousand per day** <br><br> GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day! <br> ***Andrew Knight*** <br> Managing Director, Goldman Sachs <br> [Read more](/customers/goldman-sachs/)

Read all case studies [here](/customers/)

Premium introduces priority support (4 business hour support), live upgrade assistance and a Technical Account Manager - who can aid you to achieve your strategic objectives and gain maximum value from your investment in GitLab.

# Premium Specific Features

The below list of features are after factoring in the announcement regarding [18 GitLab features moving to core](https://about.gitlab.com/blog/2020/03/30/new-features-to-core/). The timelines of the actual move of features to core in the product will be as per the linked issues in the announcement. 

#### Enterprise level Support

| **Features** |  **Value** |
| [Priority Support](https://about.gitlab.com/support/#priority-support) | Minimize outages and downtime with 4 hour response time for regular business support and 24x7 uptime support with a guaranteed 30 minute response time  |
| [Technical Account Manager](https://about.gitlab.com/services/technical-account-management/) |  GitLab account leader will help guide, plan and shape the technical deployment and implementation of GitLab, and partner to help you get the best value possible out of your relationship with GitLab. |
| [Live Upgrade Assistance](https://about.gitlab.com/support/) | Schedule an upgrade time with GitLab. We’ll join a live screen share to help you through the process to ensure there aren't any surprises.  |

### Achieve High Availability and Disaster Recovery

| Achieve reliability and performance of your DevOps service through geographic replication and HA/DR solutions. | ![Geographic Replication](https://about.gitlab.com/images/gitlab_ee/gitlab_geo_diagram.png) |

| **Features**    | **Value** |
| --------- | ------------ |
| High Availability via our [Reference Architectures](https://about.gitlab.com/solutions/reference-architectures/) | Avoid downtime and outages, ensuring developers are able to work at all times.  |
| [Disaster Recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/index.html) | Fail-over to another data center within minutes  |
| [Geographic Replication](https://about.gitlab.com/solutions/geo/) |  Reduce latency between distributed teams and increase developer productivity with globally distributed cloning and container registry geographic replication.  |

#### Developer Productivity

| Ensure your development teams are always able to be productive.  Streamline login, avoid downtime, minimize outages and reduce latency between distributed teams.  | ![Productivity Analytics](https://about.gitlab.com/images/12_3/productivity_analytics.png)  |

| **Features** |  **Value** |
| [Productivity Analytics](https://docs.gitlab.com/ee/user/analytics/productivity_analytics.html) | Analyze graphs and reports to understand team, project, and group productivity for uncovering patterns and best practices to improve overall productivity.   |
| [Merge Request Reviews](https://docs.gitlab.com/ee/user/discussions/index.html#merge-request-reviews-premium) | Track and manage code reviews and feedback with built in merge request reviews |
| [Group and File Templates](https://docs.gitlab.com/ee/user/group/#group-file-templates-premium) | Establish consistent and standard practices |
| [Smart Card Authentication](https://docs.gitlab.com/ee/administration/auth/smartcard.html) | Simplify and streamline logon process to utilize authentication via smartcard   |


#### Streamline Project Planning

|  Manage [multiple agile projects (programs)](https://about.gitlab.com/solutions/agile-delivery/) with intuitive and easy to use dashboards and reports to track issues and milestones across multiple projects.  |  ![Assignee Lists](https://docs.gitlab.com/ee/user/project/img/issue_board_assignee_lists.png)  |

| **Features**    | **Value** |
| --------- | ------------ |
| [Roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/) | Visualize the flow of business initiatives across time in order to plan when future features will ship.   |
| [Single Level Epics](https://docs.gitlab.com/ee/user/group/epics/) | Manage your portfolio of projects more efficiently by tracking groups of issues that share a theme, across projects and milestones.   |
| [Group Backlog management](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) | Simplify tracking, scoping and planning future work with group level backlog management on multiple issue boards.   |
| [Group Milestone Boards/Lists](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) | Visualize  future work to be delivered in future releases/milestones.   |
| [Assignee Boards/Lists](https://docs.gitlab.com/ee/user/project/issue_board.html) |  Streamline assignment of work to team members in a graphical assignment board.   |
| [Group Issue Boards](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) |  Visually manage programs (groups) with multiple issue boards where work can be dynamically assigned and tracked.  |
| [Issue Analytics](https://docs.gitlab.com/ee/user/group/issues_analytics/index.html) | Establish consistent and standard practices |

#### Deploy with confidence

| Accelerate software delivery with integrated deployment and release management. | ![Multiple Project Pipeline Graphs](https://docs.gitlab.com/ee/ci/img/multi_project_pipeline_graph.png) |
| Maintain an end to end picture of how your applications are deployed and delivering business value.  | [![Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/img/index_operations_dashboard_with_projects.png)](https://docs.gitlab.com/ee/user/operations_dashboard/img/index_operations_dashboard_with_projects.png) |

| **Features**    | **Value** |
| --------- | ------------ |
| [Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/index.html#doc-nav) | a holistic view of the overall health of your company's operations.  |
| [Multi Project Pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipeline_graphs.html) | Link CI pipelines from multiple projects to deliver integrated solutions |
| [Protected Environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html) | Establish controls and limit the access to change specific environments |
| [Browser Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html) | Detect performance regressions for web apps before merging into master. |


#### Manage the Development Process

| Simplify compliance with and traceability with enterprise features built into the developer's workflow. | ![Merge Request Reviews](https://docs.gitlab.com/ee/user/discussions/img/review_comment_quickactions.png) |

| **Features**    | **Value** |
| --------- | ------------ |
| [Audit Logs](https://docs.gitlab.com/ee/administration/audit_events.html) | Review changes by user and track access |
| [Auditor users](https://docs.gitlab.com/ee/administration/auditor_users.html) | Read-only access to all projects, groups, and other resources on the GitLab instance |
| [Merge Request Reviews](https://docs.gitlab.com/ee/user/discussions/index.html#merge-request-reviews-premium) | Draft multiple comments in a merge request code review and review/resolve comments together   |
| [Verified Committer](https://docs.gitlab.com/ee/push_rules/push_rules.html#enabling-push-rules) | Ensure only authorized and verified team members are allowed to commit to the project   |
| [Require Signed Commits](https://docs.gitlab.com/ee/push_rules/push_rules.html#enabling-push-rules) | Enforce policy to require signed commits from contributors  |

<center><a href="/sales" class="btn cta-btn orange">Contact sales and learn more about GitLab Premium</a></center>
