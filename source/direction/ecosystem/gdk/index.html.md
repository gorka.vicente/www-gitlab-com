---
layout: markdown_page
title: "Category Direction - GDK"
---

- TOC
{:toc}

| ------ | ------ |
| **Stage** | [Create](/direction/create) | 
| **Maturity** | [Viable](/handbook/product/categories/maturity/) |
| **Last reviewed** | 2020-05-07 |

<!-- - [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations) -->
<!-- - [Epic List](https://gitlab.com/groups/gitlab-org/-/epics/1515) -->
- [Overall Direction](/direction/ecosystem/)

_This direction is constantly evolving, and [everyone can contribute](#contributing):_

* Please comment, thumbs-up (or down!), and contribute to the linked issues and 
  epics on this category page. Sharing your feedback directly on GitLab.com is 
  the best way to contribute to our vision.
* Please share feedback directly via [email](mailto:contributors@gitlab.com), 
  [Twitter](https://twitter.com/gitlab). There's also a [Gitter contributor channel](https://gitter.im/gitlabhq/contributors) you can give us feedback and ask questions in.
* If you're a GDK user, we'd especially love to hear from you.

### Overview

The GDK category is responsible for the GitLab Development Kit (GDK). The GDK 
provides a self-contained GitLab environment, which can be run locally, or in a 
virtual machine. It's useful for developers contributing to the [GitLab project](https://gitlab.com/gitlab-org/gitlab), 
or anyone needing to test, experiment with, or validate GitLab functionality.

This team is responsible for improving the usability and reliability of the 
the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit). 
The GDK is essentially required to locally develop and test changes, before 
submitting them upstream to GitLab. It is used by nearly all of the product 
development organization at GitLab, our 
community contributors and partners. It is therefore critical to optimize this 
experience for two reasons: productivity gains made through tool enhancements 
have compounding returns, and by making it easier to get started "lowers the 
bar" to contributing. Our first focus in this area is to do a thorough 
evaluation of the challenges and opportunities for the GDK, before recommending 
a path forward.

In the longer term, the vision is for the GDK to be a simple, reliable, and 
flexible tool that allows everyone to contribute. Developers should be able to 
keep their local development environment up-to-date painlessly, designers 
should be able to quickly check out a branch and validate a feature they 
designed, and everyone using the GDK should be running few commands to achieve 
this, and experience a highly-performant local installation when they do so.

#### Target audience

The GDK serves as a simple way to set up a local development environment for 
working on GitLab. With that in mind, the target audience for the GDK is
_anyone who is contributing to the GitLab project_. This includes:

* **Software engineers** directly writing code
* **Software engineers in test** who are testing that code
* **Product designers** verifying designs are implemented correctly
* **UX researchers** demonstrating functionality to users for research purposes
* **Product managers and engineering managers** performing acceptance testing 
  before code is merged to master
* **All of the above**, whether they are working for GitLab or are part of 
  our broader community of contributors.

GitLab is an [Open Core](https://en.wikipedia.org/wiki/Open-core_model) product, 
and our community is central to the continued success of our mission. This 
means that tools that allow anyone to jump in and contribute are vital to 
keeping that momentum going.

#### Challenges to address

**Getting started with the GDK** is straightforward for GitLab employees because 
of the tribal knowledge and in-company training we do. We need to move that 
knowledge in to the documentation and the handbook to make it just as easy for 
anyone in the community to get ramped up.

**Examining a specific branch** is a central workflow for many of our personas. 
These users would benefit greatly from a minimal-fuss, no-configuration way to 
switch to a specific branch to verify various aspects of the application with 
high velocity.

**Making GDK updates a smooth experience** is a high-priority issue, as there 
are a large number of reports of breaks happening when upgrading. This makes it 
painful to upgrade and smoothly continue working, or (even worse) discourages 
developers from upgrading in a timely manner.

**Getting help with the GDK** isn't so bad for GitLab Team Members, since they 
can just join the **#gdk** Slack channel. This 
approach is not suitable for community members, and we need a better solution 
that works well for everyone (so that everyone can contribute.)

#### What's Next & Why

As a new Category supporting an existing project, the first step is getting our 
house in order. Over the next few milestones, we'll be [running a survey](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/836)
to better understand the current state of affairs with our users, establishing 
an official board that we'll be working from when planning our work, and 
grooming our existing [backlog of issues](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues) 
to make sure the things that are in there are relevant and up-to-date.

[Allow some services to be run under Docker](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/867) -
Convert "data-oriented" services to have the option of being run natively or via
Docker to reduce the complexity of installing/upgrading indivdual services.

[Run `gdk doctor` in CI](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/865). 
`gdk doctor` is the go-to tool for contributors when facing issues, but we 
currently don't run it in our CI pipeline for testing.

[Introduce `gdk pristine` command](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/843) to allow contributors a way to get a potentially broken GDK back to a working 
state in a low-touch way.

[Include 'stale process' detection as part of `gdk doctor`](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/856). This has bitten a few times recently with 
some existing services that were re-engineered. It would have been really 
helpful for `gdk doctor` to detect and suggest solutions when it detects 
potential conflicts with old services that were left running after an update.

[Update the GDK Documentation](https://gitlab.com/groups/gitlab-org/-/epics/3218)
to increase clarity particularly for new contributors. This will include 
reorganizing content to make it more discoverable and scannable, improving our 
_"Preparing your computer"_ recommendations, and updating the documentation 
for the latest requirements and prerequisites.

#### What is Not Planned Right Now

**Working on the GitLab Compose Kit (GCK).** The [GCK project](https://gitlab.com/gitlab-org/gitlab-compose-kit) 
is also building local development environment tooling, however with an aim of
allowing users to spin up a container-based environment that only runs the bare 
minimum of services required to maximize performance and ease of use. Whatever 
the future may hold, the current standard for our developers is the _GDK_, which 
is where the focus of this category will remain for the time being.

### Why is this important?

The GDK is key to our [GitLab's Strategy](https://about.gitlab.com/company/strategy/#how)
as it is the entrypoint to contribution to the GitLab project itself. Without 
the GDK, many contributors would have to learn the specifics of every required 
service that a local environment must run to function, the commands required to 
run each of those services, and the order they must be stood up in to function 
properly. They'd have to run these commands manually, or more likely, write some 
tooling that automates the process for them... which after significant amounts 
of time and effort were put in to those scripts, they'd look a lot like the GDK.
