---
layout: markdown_page
title: Product Direction - Growth
description:
---

## Growth Section Overview

The growth team at GitLab was formed as of August 2019 and we are iterating along the way. We are excited by the unique opportunity to apply proven growth approaches and frameworks to a highly technical and popular open source product. Our goal is to become a leading B2B product growth team and share our learnings internally and externally.

Currently, our growth team consists of 6 groups (acquisition, conversion, retention, expansion, fulfillment, telemetry), each consisting of a cross-functional team of Growth Product Managers, Developers, UX/Designers, with shared analytics, QA and user research functions.

### Growth Team Principles

In essence, we are a product team with a unique focus and approach. 

##### Principle 1 Growth team focuses on connecting our users with our product value

While most traditional product teams focus on creating value for users via shipping useful product features, the growth team focuses on connecting users with that value.  We do so by: 
* Drive feature adoption by removing barrier and providing guidance
* Lower Customer Acquisition Cost (CAC) by maximizing conversion rate along user journey
* Increase Life Time Value (LTV) by increasing retention & expansion
* Lower sales/support cost by using product automation to do the work

##### Principle 2 Growth team sits in the intersection of Sales, Marketing, Customer success & Product

Growth Team is by nature cross-functional and interacts and collaborates closely with many other functional teams, such as Product, Sales, Marketing, Customer success etc. Much of growth team's work helps to compliment and maximize these team's work, and ultimately allow customers to get value from our product. 

![growth team helps](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/ece3ead382dbd91201f8e09246ecdc5c6c415643/source/images/growth/growthhelps.png)


To provide some examples:
* While Marketing Team works on creating awareness and generating demand via channels such as webinars, contents & paid ads, Growth team can help lower friction in signup and trial flow to maximize the conversion rate, thus generating higher ROI on marketing spend
* While Sales Team work on convert larger customers in a high touch manner, Growth team can help create self-serve flows and funnels to convert SMB customers, as well as building automated tools to make sales process more efficient
* While Core product Team works on building features to solve customer problem and pain points, Growth team can help drive product engagement, create habit and promote multi-feature adoption
* While Customer Success & Support Team works on solving customers problems and driving product adoption via human interaction, Growth Team can help build new user onboarding flows and on-context support to serve some of these needs programmatically at large scale

##### Principle 3 Growth team uses a data-driven and systematic approach to drive growth

Growth Team uses a highly data & experimental driven approach
* We start from clearly defining what is success and how to measure it, by picking a [North Star Metric](https://about.gitlab.com/handbook/product/metrics/#north-star-metric) - the one metric that matters most to the entire business now
* Then we break down the NSM into sub-metrics, this process is called building a growth model. We continuously iterate our thinking about GitLab growth model as we learn more about our business, but our latest thinking is captured here [GitLab Growth Model](https://docs.google.com/presentation/d/1aGj2xCh6VhkR1DU07Nm3oaSzqDLKf2i64vlJKjn05RM/edit?usp=sharing). 

![GitLab Growth Model](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/0303312efae07608a8b6077996041573637d4825/source/images/growth/growthmodel.png)


* Then by reviewing the sub-metrics, we identify the area with highest potential for improvement, and pick a focus area KPI for that.  
* Then we use the build-measure-learn framework to test different ideas that can potentially improve the focus area  KPI
* Once this focus area KPI has been improved, we go back to the growth model, and identify the next area we want to focus on 

By following this systematica process, we ensure that: 1) We know what matters most; 2) Teams can work indedenpently but their efforts will all contribute to overall growth ; 3) We always start from the things with the highest ROI - these projects with high leverage 

##### Principle 4 Growth team experiments a lot and has a “Win or Learn”  mindset

***"We never fail, hypothesis proven wrong"***

Growth team views any changes as an “experiment”, and try our best to measure the impact. With well-structured hypotheses and tests, we can obtain valuable insights that will guide us towards the right direction, even if some of the experiments are not successful. 

![GitLab Growth Process](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/0303312efae07608a8b6077996041573637d4825/source/images/growth/growthflow.png)

Below is a list of our currently running and concluded experiments. 

| Experiment Name | Team | Status | Issue Link | Results Summary |
| ------ | ------ |------ |------ |------ |
|Display security navigation in left sidebar to a cohort of net new .com signups|Conversion|Live|https://gitlab.com/gitlab-org/gitlab/-/issues/34910| NA|
|Update .com paid sign up flow|Acquistion|Live|https://gitlab.com/gitlab-org/growth/product/issues/87|  NA|
|MR with no pipepline|Expansion|Live|https://gitlab.com/gitlab-org/growth/product/issues/176| NA|
|Auto renew toggle vs. Cancel |Retention |Concluded|https://gitlab.com/gitlab-org/growth/product/-/issues/43 |https://gitlab.com/gitlab-org/growth/product/issues/162|




## Growth Team's Current Focus

### FY21 Q1 OKR

**Growth Team Only**

Director of Product, Growth: Improve the customer experience with billing related workflows. gitlab-com&263
* Director of Product, Growth KR: Make material improvements to direct signup, trial, seat true-up, upgrade, and renewal workflows, both for .com and self-hosted customers. gitlab-com/Product#725
* Director of Product, Growth KR: Drive Support Satisfaction scores, filtered for billing, to >95%. gitlab-com/Product#726
* Director of Product, Growth KR: Drive $561k in incremental IACV. gitlab-com/Product#727

Director of Product, Growth: Ensure accurate data collection and reporting of AMAU and SMAU metrics. gitlab-com&265
* Director of Product, Growth KR: Deliver AMAU and SMAU tracking with less than 2% uncertainty. gitlab-com/Product#729 

**Across Product Team**

VP of Product Management: Proactively validate problems and solutions with customers. gitlab-com&266
* VP of Product Management KR: At least 2 validation cycles completed per Product Manager. gitlab-com/Product#730

VP of Product Management: Create walk-throughs of the top competitor in each stage to compare against our own demos completed in Q4 or Q1. gitlab-com&267
* VP of Product Management KR: Deliver one recorded walk-through for each stage. gitlab-com/Product#731 




## Growth Team's 1-Year Plan

In 2020, Covid-19 has brought uncertainty and disruptions to the market. In tough economic times, all businesses need to focus on efficient growth. GitLab Growth team is perfectly positioned to help the company drive growth while improving efficiency in all fronts. Therefore our 1-Year "Wartime" Growth Team Plan has 3 themes: 

##### Theme 1 Improve customer retention to build solid foundation for growth

Existing customer retention is our life blood, especially in tough marco-economic environments. GitLab has a unique advantage with a subscription business model, however we still need to make sure we serve our customers well and minimize customer churn. On the other hand, in tough times, our customers will potentially be facing challenge and uncertainty as well, therefore it will be naturally harder to upsell them with higher tiers or more seats. By staying laser focused on retention, we can build a strong foundation to weather the storm and drive growth sustainability. 

To improve retention, Growth team has identified and will be working on the opportunities below: 


1) Continue to make customer purchasing flows such as renewal and billing process robust, efficient, transparent and smooth, as these are critical converting moments.  
We have done projects such as: 
* Clarify the options between “auto-renewal” and “cancel” 
* Provide clear information on customer licenses and usage 
* Automate renewal process to minimize sales team burden

2)  Deep dive into the customer behavior and identify leading indicators for churn. This would include projects such as:  
* Create usage based retention curve with data analytics team
* Experiment with different types of “early interventions” to reduce churn

3) Identify and experiment on drivers that can lead to better retention. 

Through analysis, we can identify which customer behavior is critical to retention, and therefore we can experiment on prompting these types of behavior among more customers. For example,  one hypothesis is that if a customer use multiple stages and features of GitLab, they are more likely to get value from a single DevOp platform, thus they are more likely to become a long term customer. Therefore, in FY21, one of growth team's focus will be on helping GitLab customers adopt more stages, and we'll observe whether that lead to better retention. 

![user adoption journey](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/4ea056cd8ae6c4fe0c8691809b57da86e0fa93d4/source/images/growth/journey.png)


##### Theme 2 Increase new customer conversion rate to maximize growth efficiency

In order to grow efficiently,  we also want to maximize the efficiency of new customer acquisition. For each marketing dollar we spend, we want to bring more dollars back in terms of revenue, and we also need much shorter payback time to generate cash flow that is essential for weathering storms.

As collaborator to marketing & sales team, the role growth team can play here is to aggressively analyze, test and improve new user in- product experience between about.gitlab.com, GitLab.com, customer portal etc., with the goal of converting a prospect who doesn’t know much about GitLab, to a customer who understands the value of GitLab and happily signed up for a paid plan. 

In order to achieve this goal, we start by trying to understand what are the drivers to new customer conversion, and we observed that the number of users in a team, as well the numbers of stages/features a team tried out, all seem to be correlated with higher conversion rate to a paid plan.  Accordingly, experiments and initiatives we planned in this area include: 
1) Build new customer onboarding tutorial to help them learn how to use different stages of GitLab 

2) Qualitative study to understand the reasons behind why some customers convert, while others don’t 

3) Set up customers behavior triggers to enable user marketing team to send targeted & triggered emails 


##### Theme 3 Build out infrastructures and processes to unlock data-driven growth

Data is key to a growth team’s success. Growth team uses data & analytics daily to: define success metrics, build growth models, identify opportunities, and measure progress.  So part of our focus for FY21 will be working closely with GitLab data analytics team to build out that foundation, which include: 

1) Provide knowledge & documentation of current available data sources to understand customer behaviors . And Establish framework and best practices to enable consistent and compliant data collection

2) Build out Product Usage Metrics Framework to evaluate how customers engage with certain features. For example, we have successfully finished SMAU project in Q1, and move on to North Star Metric project in Q2. 

3) Build out Customer Journey Metrics Framework to understand how customers flow through funnel, including end to end cross-functional reporting spanning marketing, sales and growth  


Along the way, we also aim to share our learnings and insights with the broader GitLab team and community, as we firmly believe growth is a whole company mission, and the success ultimately comes from constant learning & iteration & testing & sharing, as well as building a cross-functional growth enginee breaking silos. 




## Growth Team Areas of Focus

### 1. Key User workflows: 
*  Exploration on about.gitlab.com to understand GitLab product offering
*  Signup process of a free account, a trial, or a paid plan
*  Current customer renewal flow 
*  Current customer paid plan upgrade/downgrade, add-on purchase.
*  In addition, Telemetry team is focused on building frameworks to enable product analytics

### 2. Key User Focus:

#### GitLab prospect & current customers
The external user personas on GitLab.com and our Self-Managed instances are very different and should be treated as such. We will be focusing on the 2 described below. 

In addition to the personas, it's also important to understand the permissions available for users (limits and abilities) at each level. 
* [Handbook page on permissions in GitLab](https://about.gitlab.com/handbook/product/#permissions-in-gitlab)
* [Docs on permission on docs.gitlab.com](https://docs.gitlab.com/ee/user/permissions.html)
* *Note: GitLab.com and Self-Managed permission do differ.*  

#### GitLab Team Members
*  Sales Representatives
*  Customer Success Representatives
*  Support Representatives

### 3. Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [Customers.GitLab](https://about.gitlab.com/handbook/engineering/projects/#customers-app)
*   [License.GitLab](https://about.gitlab.com/handbook/engineering/projects/#license-app)
*   [About.GitLab.com](https://about.gitlab.com/)



## What's Next

12.10


* [Acquisition] Pricing page test
* [Retention] Update the Renewal Banner Appearance, Targeting, and Dismissal Behavior
* [Retention] What's New MVC
* [Retention] Create Additional Renewal Emails
* [Retention] Update .com Billing Page for Auto-renew Case
* [Retention] Better Error Messaging When Applying a License
* [Retention] Add a link to the portal on /admin/license page
* [Expansion] Improve Purchase CI minutes screen
* [Expansion] Add a notification dot when 'buy ci minutes' button is displayed in the user settings on GitLab.com
* [Expansion] Add a 'buy ci minutes' option in the top right drop down (user settings menu) on GitLab.com
* [Telemetry] Query Optimizations: gitlab-org/telemetry#308
* [Telemetry] Harden Usage Ping gitlab-org/telemetry#335
* [Telemetry] Documentation: gitlab-org/telemetry#307


13.0 (Not finalized)
* [Retention] What's New
* [Retention] Apply Cancel Flow to For Self-Managed Subscriptions
* [Telemetry] Any other enhancements deemed necessary for Usage Ping in support of SMAU/AMAU after 12.10
* [Telemetry] Hybrid Logs and Postgres Events w/ Usage Ping: gitlab-org/telemetry#333
* [Telemetry] Add page_type to custom context in our snowplow pageview tracker: gitlab-org/telemetry#110


## Growth Issue Board

* [Acquisition board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition)
* [Conversion board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion)
* [Expansion Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) 
* [Retention Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aretention)
* [Fulfillment Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Afulfillment) 
* [Telemetry Board](https://gitlab.com/gitlab-org/telemetry/-/boards) 


### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
