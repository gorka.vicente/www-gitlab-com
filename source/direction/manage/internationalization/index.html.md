---
layout: markdown_page
title: "Category Direction - Internationalization"
---

- TOC
{:toc}

Last Reviewed: 2020-05-04

## Introduction

Thanks for visiting the direction page for Internationalization in GitLab. This page belongs to the [Import](https://about.gitlab.com/handbook/product/categories/#import-group) group of the [Manage](https://about.gitlab.com/direction/dev/#manage-1) stage and is maintained by [Haris Delalić](https://gitlab.com/hdelalic) who can be contacted directly via [email](mailto:hdelalic@gitlab.com). This vision is a work in progress and everyone can contribute. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/2722) for this category.

As GitLab evolves, we'd like anyone around the world to be able to use the application and contribute to it. In order for us to accomplish this together, our goal should be to translate the GitLab application and documentation into many languages. Improving the product from release to release should be building with internationalization in mind as the default (e.g. by continually externalizing strings).

Once we've accomplished several high-quality translations for the entire application, we'd also like to include our documentation and contribution guidance in our translation efforts. Eventually, we hope to expand and scale our community of contributors internationally to encourage developers around the world to contribute to GitLab.

## What's Next & Why

Internationalization is a community-driven effort maintained by the [Import group](https://about.gitlab.com/handbook/product/categories/#import-group). While the GitLab team regularly schedules [issues](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=internationalization&scope=all&sort=milestone&state=opened&utf8=%E2%9C%93) to support this project, no significant changes to our Crowdin program are currently planned. Although we may consider hiring an internationalization company to translate GitLab in the future, internationalization remains a community-driven effort that can only succeed with the involvement of the wider community.

While internationalization improvements are not being actively prioritized on the Import group, we intend on prioritizing several key improvements in the first half of 2020:
* [Externalize all strings in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/846). We've made significant improvements in our ability to detect non-externalized strings; we plan on finishing this work and planning a hackathon to complete string externalization in the application.
* [Important technical improvements that impact day-to-day development](https://gitlab.com/groups/gitlab-org/-/epics/271). Integrating translations into our development process has some significant shortfalls, which reduce quality and increase manual effort.

## How you can help

We're always looking for contributions to help us translate GitLab. [Here's how you can help](https://docs.gitlab.com/ee/development/i18n/#how-to-contribute):
1. Externalize strings in GitLab. Before we can translate a string, we first need to mark it so the application's able to retrieve an appropriate translation in another language.
1. Contribute translations at [Crowdin](https://translate.gitlab.com/). You can help contribute new translations to a variety of languages and vote up/down on existing translations.
1. Become a [proof reader](https://docs.gitlab.com/ee/development/i18n/proofreader.html). All translations are read by a [proof reader](https://docs.gitlab.com/ee/development/i18n/proofreader.html) before being accepted, and we're always looking for help across a number of languages.
