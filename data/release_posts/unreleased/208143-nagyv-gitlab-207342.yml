features:
  primary:
    - name: "GitLab-managed Terraform state files"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/user/infrastructure/'
      image_url: '/images/unreleased/terraform-gitlab-backend.png'
      reporter: nagyv-gitlab
      stage: configure
      categories:
        - 'Infrastructure as Code'
      issue_url: 'https://gitlab.com/groups/gitlab-org/-/epics/2673'
      description: |
        Terraform uses a state file to map your configuration to real-world resources and keep track of additional metadata. Starting a new Terraform project usually requires setting up a backend to store the state file in a reliable and secure way. This storage is always provided by a 3rd party as storing the state locally or in the git repo is not a recommended practice.

        Many of our users wanted a simpler way to set up their state file storage without involving additional services or setups. Starting with GitLab 12.10, you can configure GitLab once at the instance level to use a specific object storage for all Terraform state files. This way you won't need to set up state storage separately for every new project. GitLab managed Terraform state is available both for GitLab Self-Managed installations and on gitlab.com. 

        GitLab-managed Terraform state files allow a seemless experience for starting a new infrastructure project with minimal boilerplate needed and store your state files in a place controlled by the GitLab instance. The state files can be accessed using Terraform's [HTTP backend](https://www.terraform.io/docs/backends/types/http.html) and GitLab provides CI level integration that requires almost no setup from its users. This way users can migrate to the GitLab Terraform backend easily, and access it from their local terminals as well. 
