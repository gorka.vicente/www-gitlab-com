---
title: Observations on increasing merge request rate
author: David O'Regan
author_gitlab: oregand
author_twitter:
categories: unfiltered
image_title: '/images/blogimages/book.jpg'
description: At GitLab, we try to value only meaningful metrics when it comes to delivering an amazing product, the main one we use for that is the merge request rate. Let's discuss some observations about how to increase it via better iteration.
tags: Incident Management, Monitoring, Observability, Merge Requests, GitLab Values
guest: false
ee_cta: true
install_cta: true
twitter_text: "Observations on how to iterate faster"
featured:
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered/#legal-disclaimer)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

Do you know much about fighter jets? It's okay if you don't, neither did I until I became a software developer. While it seems like a rather strange set of things to see a correlation with, they are intrinsically related through a man named [John Boyd](https://en.wikipedia.org/wiki/John_Boyd_(military_strategist)) who was a military strategist and a fighter pilot.

Boyd was rather famous in the Air Force for a law he coined, which we're going to use to demonstrate the difference between iterative and recursive approaches to software development, why we favour it in the Monitor:Health team and why you might want to favour it too.

_Boyd's Law of Iteration states that **speed** of iteration beats quality of iteration_

This law was developed by Boyd while observing dogfights between MiG-15s and F-86s. Even though the MiG-15 was considered a superior aircraft by aircraft designers, the F-86 was favored by pilots. The reason it was favored was simple: in one-on-one dogfights with MiG-15s, the F-86 won nine times out of ten.

What's happening here? If the MiG is the better aircraft, why would the F-86 win the majority of the fights? Well according to Boyd who was one of the best dog-fighters in history suggested:

> That the primary determinant to winning dogfights was observing, orienting, planning, and acting **faster** not better.

This leads to Boyd's Law of Iteration: speed of iteration beats quality of iteration. Whats pretty incredible is that you will find this same scheme throughout every section of modern software development:

- Writing unit tests? Keep them small and lean so they can be run faster.
- Writing usability tests? They work best when they're kept lean and you can quickly discard what's not working.
- Writing a function, class or feature? Start with the smallest, most boring solution and iterate.
- Doing an Agile approach? The quicker the better you'll often find.
- Software in general is about failing early and often.

So lets pretend I've convinced you with some obscure fighter jet references and now you're ready to break down those merge requests and iterate quicker than you've ever iterated.  Awesome! Let's talk about how to foster a team environment that allows it because that's the key here at GitLab. When you get started on this pilgrimage to [11 amazing merge requests per month as a goal](https://about.gitlab.com/handbook/engineering/development/performance-indicators/#mr-rate) you need to keep one very important thing in mind:

It's a team effort and while you as an individual developer will do an amazing job for yourself hammering on this skill, the real difference gets made when you look at it as a tool to lift the team up. Think of yourself as the pilot that wants to get that faster iteration in to cover your buddies.

## Bias For Action

When I got started at GitLab I was introduced to the idea of really believing in iteration as a methodology because it's a [company value](https://about.gitlab.com/handbook/values/#iteration)

> Decisions should be thoughtful, but delivering fast results requires the fearless acceptance of occasionally making mistakes

This was highlighted in various ways by different people across the company but something that really stuck out to me was hearing another team member refer to the Monitor:Health team as a "team with a strong bias for action". We don't really believe in being re-active, we want to be we want to be pro-active in improving the product. This underlying system of belief in the team trickles down from our team lead into each discussion, choice made, deliverable set and ultimately how we as developers see our own agency to operate. We **believe** in action, that an open merge request (even if its not perfect) is always better than nothing.

When a problem is anticipated, before a discussion comes a merge request, then the discussion. I know for a lot of people this might seem a bit counterproductive in creating a possible waste of effort but in reality, [starting at a merge request](https://about.gitlab.com/handbook/communication/#everything-starts-with-a-merge-request) is the best possible place for any real discussion. It helps create a living log for the conversation and opens up the issues visibility.

## All Code Is Bad Code - Impostor Syndrome, Course Correction And Accepting Failure

A few years ago when I was working for a small company in Ireland, I was fortunate enough to work under a person that to this day I consider to be a great friend and my best mentor. He was everything I wanted to be as a software developer: cool, calm, collected, full stack, performance driven and so much more. He was an example in our company for other developers to aspire too, the kind of smart you wanted to embody in a profession that's mostly about logical thinking.

- Productions down? Ask him.
- We got DDOS'd? Ask him.
- Don't know a thing about unit testing? Ask him.
- Can't remember the fibonacci sequence? Ask him.
- Want to know how to divide by 0 finally? Ask him.

...you get the point.

A year into being at the company and becoming a technical specialist for the product, he gave our team a talk one Friday called "All Code Is Bad Code". This talk became rather infamous in our small team because for the duration of the talk he mostly spoke about why the majority of the code he had written himself was ultimately bad code and why the need to **appear** smart is the number 1 barrier for people becoming great software developers.

> What you make with your code is how you express yourself, not the code itself - Eric Elliott

Programming is by it's nature difficult. As humans we're not particularly well suited for deep and abstract logical thinking, our brains don't work like that by default and it's a learnt skill for the majority. Being reminded of this is a humbling but freeing experience as it helps you move forward without fear. Every merge request you submit should be high quality but your definition of high quality should shift to mean delivering something useful to an end user.

At GitLab, we accept our limitations in that we might not know everything about a problem we're trying to solve. Instead we lean heavily into the idea of the smallest, most [boring solutions](https://about.gitlab.com/handbook/values/#boring-solutions) that can be expanding upon quickly with the collaboration of the wider team.

> Our bias for action also allows us to course correct quickly.

We always accept there will be [uncertainty](https://about.gitlab.com/handbook/values/#accepting-uncertainty) in what we do as software developers but we don't let that stop us from trying to deliver a amazing product for our users.

When we offer a merge request, we do so with a [low sense of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame) and a [no ego](https://about.gitlab.com/handbook/values/#no-ego) perspective. This lets us deliver fearlessly **even if we're wrong**.

As a team, this is the environment you want to foster because it helps create a wonderfully positive feedback loop: Low sense of shame => many merge requests submitted => many discussions had => many iterations => ideally the best possible collaborative results for the end user.

If you're a team lead and you can take only one point away from this to improve your team, take away the ideal that **it's okay to make mistakes**, and that the best thing you can do as a team is foster a safe place for developers to do so.

If you're a developer and you can take only one point away from this to help your team take away the ideal that **it's okay to make mistakes as long as you strive for course correction**.

## Foster A Healthy Sense Of Urgency For Writing Things Down

> "While you're thinking about doing it...just do it."

It's one of the things we do so well at GitLab in general is writing things down. It's how we enable each other to pick up and go without needing to waste time on needless communication.

I don't know if you've seen the landing page for the GitLab handbook but it's safe to say that at [2,500,000 words](https://about.gitlab.com/handbook/about/#count-handbook-pages), the folks here take writing things down pretty seriously.

In the GitLab, we believe this is also a path to a higher merge request rate! Say what now?

Well in the Monitor:Health team and throughout GitLab we hammer on the core belief on not wasting energy, capturing valuable conversations and making them public to give that knowledge to the wider team. As a new team member, I've seen this in action multiple times now. Over the course of my 8 weeks at Gitlab, I can count on one hand the amount of times I've needed to ping a team member with a questions I could not find an answer written down for. This discipline around keeping these notes really keeps the focus on delivering results since we don't have an excess of energy spent going back and forth with questions.

- [How to set up your local GitLab Development Kit?](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/howto/auto_devops/tips_and_troubleshooting.md)
- [How to set up the GitLab Development Kit with Prometheus?](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/howto/prometheus.md)
- [How to use embedded charts via Prometheus and Grafana?](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#embedding-gitlab-managed-kubernetes-metrics)
- [How to use the `@gitlab/ui` components?](https://gitlab.com/gitlab-org/gitlab-ui/-/blob/master/README.md)
- [How to handle styling in external projects?](https://gitlab.com/gitlab-org/gitlab-ui/-/blob/master/doc/css.md)
- [How should components look and act on pages?](https://design.gitlab.com/)

If you can encourage your team to keep good documentation for issues that people experience it can make a huge difference in the amount any one developer is able to deliver.

> Documentation is a love letter that you write to your future self. - Damian Conway

## Tighten Those Feedback Loops

> Keep what works, disregard what doesn't.

You'll often notice that with tight-knit teams, the feedback loops of those teams get tighter over time. People begin to see patterns of what does and what doesn't work emerging from the hours spent working together. A good team should aim to address these patterns, keep the ones that work, refine them but also not be afraid to disregard the ones that don't work.

Recently in the Monitor:Health team we delivered the first iteration of the Incident management tool called the [Status page](https://gitlab.com/gitlab-org/status-page). Overall the team did an amazing job, with each member really aiming to break problems into their smallest pieces and iterate quickly which kept the overall merge request rate high.

What really made a massive difference however was the post mortem of the development process. We as a team had a very open and honest discussion about the aspects that worked well, and the ones that didn't with the end goal being to tighten our feedback loops to really enable people to be truly autonomous and asynchronous. This as a team takes a lot of bravery to be willing to look at the things you've done not so well and publicly talk about them, not just focus on all the things you have done well.

What does that look like practically? Well to us on the Monitor:Health team it meant getting better at the refinement of issues to ensure that when they receive a `ready for development` label they are **truly** ready for anyone to pick up at any time and take it all the way to done. This really helps increase the overall merge request rate as people don't sit through 1 - 3 feedback loops just having questions answered when they could be getting it.

Issue has a [`ready for development` label](https://about.gitlab.com/handbook/product-development-flow/#build-phase-2-develop--test)?

- It should have a clear Definition of Done
- It should have all needed conversations already conducted and inside the issue
- It should have a clear set of expectations from the developer
- It should say if it needs tests
- It should say if it needs UX

In this way, we are trying to enable **any** developer on the Monitor:Health team to arrive into an issue with zero pre-existing context and deliver a merge request related to the issue without needing to go outside that issue. Remember we're trying to [measure results not hours](https://about.gitlab.com/handbook/values/#measure-results-not-hours) and that means the less time anyone spends needing to ask questions they can spend more time delivering results!

> Hail to the issue, baby! - Duke Nukem if he was a software developer at GitLab

## It's All About The Team

I really cannot echo this enough. The only reason we are able to create this level of velocity inside the GitLab is the instilled belief that we can and **should** iterate quickly. Having the support of the team across the main points in how to iterate i.e. bias for action, low sense of shame, a healthy sense of urgency and tight feedback loops is the bedrock behind delivering results for customers via a better product.

Well, that's all folks! I hope you enjoyed the read and learned something along the way. If you have any questions or want to suggest an improvement, drop me an email at: `doregan@gitlab.com`.

When in doubt, iterate faster.

## TL;DR, show me the proof

![Results](/images/blogimages/iterate-faster/results.png){: .center}

The Monitor:Health frontend team has grown in team size over time while increasing average merge request rate. The team merge request rate reflects the current team size of 4 people.

Cover image by [Aaron Burden](https://unsplash.com/photos/G6G93jtU1vE) on [Unsplash](https:unsplash.com)
{: .note}

## References

- [GitLab Values](about.gitlab.com/handbook/values)
- [Boyds Law](https://blog.codinghorror.com/boyds-law-of-iteration/)
- [All code is bad](https://www.stilldrinking.org/programming-sucks)
- [Accepting failure](https://www.youtube.com/watch?v=UxvXgmZf6NU)

[Join us](/jobs/apply/) at GitLab! Or consider [trying us out](/free-trial/) for free.

DISCLAIMER: This blog is intended for user-generated content submitted by the GitLab team. The views and opinions represented in this blog are personal to the author of each respective blog post and do not represent the views or opinions of GitLab unless explicitly stated. All content provided on this blog is for informational purposes only. Neither GitLab nor any of the individual blog contributors ("Contributors") make any representations as to the accuracy or completeness of any information on this site. Neither GitLab nor any Contributors will be liable for any errors or omissions in this information or any losses, injuries, or damages from the display or use of this information. Comments are welcome, and in fact, encouraged. However, GitLab reserves the right to edit or delete any comments submitted to this blog without notice should GitLab determine them to i) be spam or questionable spam; ii) include profanity; iii) include language or concepts that could be deemed offensive, hate speech, credible threats, or direct attacks on an individual or group; or iv) are in any other way a violation of GitLab's Website Terms of Use. GitLab is not responsible for the content in comments. This policy is subject to change at any time.

<%= partial "includes/blog/blog-merch-banner" %>
